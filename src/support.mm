#############################################################################
#                                                                           #
# Copyright (C) 2018 by Claus Christensen                                   #
# This file is part of KGModul                                              #
#                                                                           #
# This program is free software: you can redistribute it and/or modify      #
# it under the terms of the GNU General Public License as published by      #
# the Free Software Foundation, either version 3 of the License, or         #
# (at your option) any later version.                                       #
#                                                                           #
# This program is distributed in the hope that it will be useful,           #
# but WITHOUT ANY WARRANTY; without even the implied warranty of            #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
# GNU General Public License for more details.                              #
#                                                                           #
# You should have received a copy of the GNU General Public License         #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    #
#                                                                           #
#############################################################################

###################
# 
# Fejl(s [, args])
# 
# Kalder error
#
# s er en fejlmeddelelse
# args er objekter, der indsættes i fejlmeddelsen
#
# Workaround: man kan ikke bruge cat(...) som første argument til error,
#  så der er ingen god måde at skrive lange fejlmeddelser direkte i koden
#
Fejl := proc(s)
    error(s, _rest);
end proc;


###################
# 
# Err_(s1, ... [, args])
# 
# Kalder Fejl
#
# s1,... er en liste af strenge, som konkateneres til en fejlmeddelse
# args er objekter, der indsættes i fejlmeddelsen
#
# Workaround: Nødvendigt med et ekstra funktionskald for at sikre, at err
#  kaldes med en streng - en formel parameter evalueres før indsættelse i 
#  error i modsætning til en lokal variabel.
#
Err_ := proc(strs::seq(string))
    local msg_;
    
    msg_ := cat(strs);
    Fejl(msg_, _rest);
end proc;

    
###################
# isFloat_(udtryk)
#
# true, hvis udtryk er et float eller et aritmetisk udtryk, som indeholder et float
#
isFloat_ := proc(x)
    local y;
    if type(x, 'numeric') then
        return type(x, 'float');
    elif type(x, `+`) or type(x, `*`) or type(x, `^`) or type(x, 'function') then
        for y in op(x) do
            if isFloat_(y) then
                return true;
            end if;
        end do;
    end if;
    false;
end proc;


###################
# AfrundTal_(udtryk, decimaler)
#
# Hjælpefunktion til Afrund_: Tal med for få decimaler får tilføjet 0'er
#
AfrundTal_ := proc(udtryk, decimaler::integer)
    local d_, u_, e_;
    
    if udtryk = 0 then      # 0 kræver særbehandling; kan kun skrives som 0.
        return udtryk;
    end;
    
    u_ := op(1, udtryk);    # tallets decimaler (uden komma, med fortegn)
    e_ := op(2, udtryk);    # kommaets placering regnet fra bagerste ende
    d_ := decimaler - ceil(log[10](abs(u_)));
    if type(log[10](abs(u_)), 'integer') then     # +/- 10^n kræver særbehandling
        d_ := d_ - 1;
    end if;
    
    if d_ > 0 then          # der mangler nogle 0'er efter kommaet
        Float(u_ * 10^d_, e_ - d_);
    else
        udtryk;
    end if;
end proc;


###################
# Afrund_(udtryk [, 'cifre' = n])
#
# Som evalf. Der afrundes til n betydende cifre
# Da evalf først afrunder hver enkelt komponent af et komplekst udtryk og så beregner
# det komplekse udtryk med afrundede værdier, så er det vigtigt først at bruge ekstra
# afrundingsnøjagtighed
#
Afrund_ := proc(udtryk, decimaler::integer := -1)
    local decimaler_, udtryk_;
    
    if decimaler >= 0 then
        decimaler_ := decimaler;
    else
        decimaler_ := FindOption_('cifre', _rest);
        if decimaler_ = FAIL then       # til kald fra Løs, som bruger cifre som variabel
            decimaler_ := FindOption_('_cifre', _rest, '_def'=-1);
        end if;
    end if;
    
    if decimaler_ > 0 then
        # Afrund først med ekstra decimaler; ellers afrundes sammensatte udtryk forkert
        udtryk_ := evalf[decimaler_ + 10](udtryk);
        udtryk_ := evalf[decimaler_](udtryk_);
        if type(udtryk_, 'realcons') then
            AfrundTal_(udtryk_, decimaler_);
        # måske blot type(udtryk_ ...
        elif nops(udtryk_) = 2 and type(op(2, udtryk_), 'with_unit') then
            AfrundTal_(op(1, udtryk_), decimaler_) * op(2, udtryk_);
        else
            udtryk_;
        end if;
    else
        udtryk;
    end if;
end proc;

###################
# SaveName_(navn, ext)
#
# Returnerer den fulde sti til en fil, der skal gemmes eller hentes
# Data-mappen oprettes, hvis den ikke allerede findes
#
# - navn er en variabel eller streng
# - ext er en valgfri filendelse. def = .mps
#
SaveName_ := proc(navn, ext::string := ".mps")
    local home_, dir_;
    
    use FileTools in
        home_ := kernelopts('homedir');
        dir_ := JoinPath([home_, "KGModulData"]);
        if not Exists(dir_) then
            mkdir(dir_);
        end if;
        JoinPath([dir_, cat(convert(navn,'string'), ext)]);
    end use;
end proc;
	

###################
# 
# Cifre_
# 
# Returnerer værdien givet som 'cifre=<værdi>'
# Hvis cifre= ikke findes, så bruges _def; ellers Digits
#
Cifre_ := proc()
    if HasOption_(_def, _rest) then
        FindOption_('cifre', _rest);
    else
        FindOption_('cifre', _rest, '_def' = Digits);
    end if;
end proc;


###################
# 
# Some_(pred, list)
# 
# true, hvis et element i list opfylder predikatet pred; ellers false
#
# - pred er en boolsk procedure/funktion af en variablen af samme type som elementerne i list
# - list er en liste
#
Some_ := proc(pred::procedure, lst::list)
    local elt;
    for elt in lst do
        if pred(elt) then
            return true;
        end if;
    end do;
    return false;
end proc;


###################
# 
# None_(pred, list)
# 
# true, hvis intet element i list opfylder predikatet pred; ellers false
#
# - pred er en boolsk procedure/funktion af en variablen af samme type som elementerne i list
# - list er en liste
#
None_ := proc(pred::procedure, lst::list)
    local elt;
    for elt in lst do
        if pred(elt) then
            return false;
        end if;
    end do;
    return true;
end proc;


###################
# 
# All_(pred, list)
# 
# true, hvis alle elementer i list opfylder predikatet pred; ellers false
#
# - pred er en boolsk procedure/funktion af en variablen af samme type som elementerne i list
# - list er en liste
#
All_ := proc(pred::procedure, lst::list)
    local elt;
    for elt in lst do
        if not pred(elt) then
            return false;
        end if;
    end do;
    return true;
end proc;
