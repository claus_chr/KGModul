#############################################################################
#                                                                           #
# Copyright (C) 2018 by Claus Christensen                                   #
# This file is part of KGModul                                              #
#                                                                           #
# This program is free software: you can redistribute it and/or modify      #
# it under the terms of the GNU General Public License as published by      #
# the Free Software Foundation, either version 3 of the License, or         #
# (at your option) any later version.                                       #
#                                                                           #
# This program is distributed in the hope that it will be useful,           #
# but WITHOUT ANY WARRANTY; without even the implied warranty of            #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
# GNU General Public License for more details.                              #
#                                                                           #
# You should have received a copy of the GNU General Public License         #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    #
#                                                                           #
#############################################################################

use Statistics in

    ###################
    # 
    # BinomP(p, n, r)
    # 
    # Beregner binomialfordelingen
    # - p sandsynlignedsparameteren
    # - n antalsparameteren
    # - r den variable
    #
    BinomP := proc( p, n, r )
        ProbabilityFunction( RandomVariable( Binomial(n,p)), r);
    end proc;

    ###################
    # 
    # BinomK(p, n, r)
    # 
    # Beregner den kumulerede binomialfordeling
    # - p sandsynlignedsparameteren
    # - n antalsparameteren
    # - r den variable
    #
    BinomK := proc( p, n ,r )
        CDF( RandomVariable( Binomial(n,p)), r);
    end proc;

    ###################
    # 
    # NormalP(m, s, x)
    # 
    # Beregner normalfordelingen
    # - m middelværdien
    # - s spredningen
    # - x den variable
    #
    NormalP := proc( m, s, x )
        PDF( RandomVariable( Normal(m,s)), x);
    end proc;

    ###################
    # 
    # NormalK(m, s, x)
    # 
    # Beregner den kumulerede normalfordeling
    # - m middelværdien
    # - s spredningen
    # - x den variable
    #
    NormalK := proc( m, s, x )
        CDF( RandomVariable( Normal(m,s)), x);
    end proc;
    
    ###################
    # 
    # Chi2P(n, x)
    #
    # Beregner Chi-i-anden-fordelingsfunktionen
    # - n antal frihedsgrader
    # - den variable
    #
    Chi2P := proc(n, x)
        PDF( RandomVariable( ChiSquare(n)), x);
    end proc;

    ###################
    # 
    # Chi2K(n, x)
    #
    # Beregner den kumulerede Chi-i-anden-fordelingsfunktion
    # - n antal frihedsgrader
    # - den variable
    #
    Chi2K := proc(n, x)
        CDF( RandomVariable( ChiSquare(n)), x);
    end proc;

    ###################
    # 
    # FraktiPlot(data)
    #
    # Laver et fraktilplot
    # - data er en liste eller vektor med data
    # - tilvalg: cifre = n
    #
    FraktilPlot := proc(data)
        local mu_, sigma_;
        mu_ := Mean(data);
        sigma_ := StandardDeviation(data);
        Plot_(NormalPlot(data),
            Line_(mu, " = ", Afrund_(mu_, _rest)),
            Line_(sigma, " = ", Afrund_(sigma_, _rest))
        );
        mu_, sigma_
    end proc;
    
end use;

###################
# 
# Value_(val, cif)
# 
# Returnerer val, evt. afrundet afhængigt af cif
#
# - val er en størrelse
# - cif angiver antallet af betydende cifre som val skal afrundes til
#       Hvis cif = 0 returneres val uafrundet
#       Hvis cif < 0 afrundes til heltallig værdi
#
Value_ := proc(val, cif)
    if cif > 0 then
        evalf[cif](val);
    elif cif < 0 then
        round(val);
    else
        val;
    end if;
end proc:

###################
# 
# Chi2Out_(res, cifre [, opts])
# 
# Returnerer formatteret tekst for Chi2Test og Chi2Uafhængighen
#
# - res er en liste med resulater fra Maples ChiSquare.. funktioner
# - cifre angiver antal betydende cifre, som resultater skal vises med
# - opts kan være
#       'p' angiver,at p-værdies skal udskrives
#       'k' angiver at den kritiske værdi skal udskrives
#       'teststørrelse' angiver, at chi^2-værdien udskrives
#       'frihedsgrader' angiver, at antallet af frihedsgrader udskrives
#
Chi2Out_ := proc(res, cifre)
    local _str, _strings;
    _strings := NULL;

    if HasOption_('p', _rest) then
        _strings := _strings, Line_(
                "p-værdien er ", Value_(res[2], cifre)
            );
    end if;
    if HasOption_('k', _rest) then
        _strings := _strings, Line_(
                "Den kritiske værdi er ", Value_(res[3], cifre)
            );
    end if;
    if HasOption_('teststørrelse', _rest) then
        _strings := _strings, Line_(
                "Teststørrelsens værdi er ", Value_(res[4], cifre)
            );
    end if;
    if HasOption_('frihedsgrader', _rest) then
        if op(res[5]) > 1 then
            _str := "er";
        else
            _str := ""
        end if;
        _strings := _strings, Line_(
                "Testen har ", op(res[5]) , " frihedsgrad", _str
            );
    end if;
    _strings
end proc;

###################
# 
# Chi2Fit(M [, opts])
# 
# Undersøger om et sæt observationer stammer fra en given fordeling
#
# - M er en 2 x n-matrix: 1. række er observationer, 
#                         2. række er forventede tal
# - opts kan være
#       'cifre'=n antal betydende cifre, som resultater vises med
#       'signifikans'=p (def = 5) angiver 
#       'tekst' angiver, at konsklusionen skal udskrives
#       'p', 'k', 'teststørrelse', 'frihedsgrader' - se Chi2Out_
#
Chi2Fit := proc( M::Matrix )
    local _sig, _opts, _res, _cifre, _strings;
    _strings := NULL;
    _cifre := Cifre_( _rest, '_def' = 0);
    _sig := FindOption_('signifikans', _rest, '_def' = 5);
    _opts := [
        'hypothesis', 'pvalue', 'criticalvalue', 
        'statistic', 'distribution'
    ];
    _res := [
        Statistics:-ChiSquareGoodnessOfFitTest(
            Array(M[1]), Array(M[2]), 'level' = _sig/100.0, 'output' = _opts
        )
    ];
    if HasOption_('tekst', _rest) then
        if _res[1] then
            _strings := _strings, Line_(
                "Nulhypotesen kan ikke forkastes på ", _sig, "% signifikansniveau"
            );
        else
            _strings := _strings, Line_(
                "Nulhypotesen må forkastes på ", _sig, "% signifikansniveau"
            );
        end if;
    end if;
    _strings := _strings, Chi2Out_(_res, _cifre, _rest);
    if not _strings = NULL then
        Print_(_strings);
    end if;
    op(_res[2..-2]);
end proc;

###################
# 
# Chi2Uafhængighed(M [, opts])
# 
# Undersøger om to sæt observationer stammer fra samme fordeling
#
# - M er en 2 x n-matrix: 
#
# - opts kan være
#       'cifre'=n antal betydende cifre, som resultater vises med
#       'forventet' udskriver en tabel over forventede værdier
#               tallene afrundes som til 'decimaler' (def -1 = heltal)
#       'signifikans'=p (def = 5) angiver 
#       'tekst' angiver, at konsklusionen skal udskrives
#       'p', 'k', 'teststørrelse', 'frihedsgrader' - se Chi2Out_
#
Chi2Uafhængighed := proc( M::Matrix )
    local _sig, _opts, _res, _rk_sum, _kol_sum, _sum, _i, _j, _n_rk, _n_kol, _forv, _rows,
            _cifre, _strings, _T;
    _strings := NULL;
    _T := NULL;
    _cifre := Cifre_( _rest, '_def' = 0);
    if HasOption_('forventet', _rest) then
        _n_rk := op(1,M)[1];
        _n_kol := op(1,M)[2];
        for _i from 1 to _n_rk do
            _rk_sum[_i] := 0;
            for _j from 1 to _n_kol do
                _rk_sum[_i] := _rk_sum[_i] + M[_i, _j];
            end do;
        end do;
        for _j from 1 to _n_kol do
            _kol_sum[_j] := 0;
            for _i from 1 to _n_rk do
                _kol_sum[_j] := _kol_sum[_j] + M[_i, _j];
            end do;
        end do;
        _sum := 0;
        for _i from 1 to _n_rk do
            _sum := _sum + _rk_sum[_i];
        end do;
        use DocumentTools, DocumentTools:-Layout in
            _rows := NULL;
            for _i from 1 to _n_rk do
                for _j from 1 to _n_kol do
                    _forv[_j] := Cell(Textfield(Equation(round(_rk_sum[_i]*_kol_sum[_j]/_sum))));
                end do;
                _rows := _rows, Row(op(convert(_forv, 'list')));
            end do;
            _T := Table('title'="Forventede værdier", 'drawtitle'=true, 
                    'captionposition'='above', 'captionalignment'='center', 
                    _rows);
        end use;
    end if;
    _sig := FindOption_(signifikans, _rest, '_def' = 5);
    _opts := [
        'hypothesis', 'pvalue', 'criticalvalue', 
        'statistic', 'distribution'
    ];
    _res := [
        Statistics:-ChiSquareIndependenceTest(
            M, 'level' = _sig/100.0, 'output' = _opts
        )
    ];
    if HasOption_('tekst', _rest) then
        if _res[1] then
            _strings := Line_(
                    "Det kan ikke afvises på ", _sig, "% signifikansniveau, ",
                    "at fordelingerne er ens"
                );
        else
            _strings := Line_(
                    "Det må afvises på ", _sig, "% signifikansniveau, ",
                    "at fordelingerne er ens"
                );
        end if;
    end if;
    _strings := _strings, Chi2Out_(_res, _cifre, _rest);
    if not _T = NULL then
        use DocumentTools, DocumentTools:-Layout in
            InsertContent(Worksheet(_T, DocumentBlock(Group(Input(_strings)))));
        end use;
    elif not _strings = NULL then
        Print_(_strings);
    end if;
    op(_res[2..-2]);
end proc;

