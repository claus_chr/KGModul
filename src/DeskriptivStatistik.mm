#############################################################################
#                                                                           #
# Copyright (C) 2018 by Claus Christensen                                   #
# This file is part of KGModul                                              #
#                                                                           #
# This program is free software: you can redistribute it and/or modify      #
# it under the terms of the GNU General Public License as published by      #
# the Free Software Foundation, either version 3 of the License, or         #
# (at your option) any later version.                                       #
#                                                                           #
# This program is distributed in the hope that it will be useful,           #
# but WITHOUT ANY WARRANTY; without even the implied warranty of            #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
# GNU General Public License for more details.                              #
#                                                                           #
# You should have received a copy of the GNU General Public License         #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    #
#                                                                           #
#############################################################################

###################
# 
# TjekData_(M)
# 
# Hvis M er 2xn eller nx2 så beregnes frek. og kum.frek.
#
TjekData_ := proc( M::Matrix )
    local data_;
    if op(1, M)[1] = 2 or op(1, M)[2] = 2 then
        data_ := LavTabel(M);
    else
        data_ := M;
    end if;
    data_;
end proc;


###################
# 
# TilpasData_(M)
# 
# M antages at være 2xn eller nx2. Returnerer M som 2xn-matrix
#
TilpasData_ := proc( M::Matrix )
    if op(1, M)[1] > 2 then # data i søjler
        M^+;
    else
        M;
    end if;
end proc;


###################
# 
# FindMatrix_(M)
# 
# Returnerer matricen
# M - matrix eller liste (fra LavTabel)
#
FindMatrix_ := proc(M)
    if type(M, 'list') then
        M[2];
    else
        M;
    end if;
end proc;


###################
# 
# Antal_(M)
# 
# Returnerer antallet af datapunkter i matricen
#
Antal_ := proc(M)
    local M_;
    if type(M, 'list') then
        M_ := M[2];
    else
        M_ := M;
    end if;
    op(1, M_)[2];
end proc;


###################
# 
# Akser_(M [,'x'=min..max] [,'y'=min..max])
# 
# Returnerer range til x- og y-akse. Hvis y-aksen ikke gives eksplicit 
#   returneres x-range, FAIL.
# Bruges kun i fb.m. ikke-grupperede observationer
#
Akser_ := proc(M)
    local xinterval_, yinterval_, M_, xmin_, xmax_, pad_;
    xinterval_ := FindOption_('x', _rest);
    yinterval_ := FindOption_('y', _rest);
    M_ := M[2];
    if xinterval_ = FAIL then
        xmin_ := M_[1, 1];
        xmax_ := M_[1, Antal_(M_)];
        pad_ := (xmax_ - xmin_) * 0.1;
        xinterval_ := (xmin_ - pad_)..(xmax_ + pad_);
    end if;
    xinterval_, yinterval_;
end proc;


###################
# 
# FindFraktil_(p, M, min_, maks)
# 
# Returnerer p%-fraktilen (eller _undef, hvis p<0 eller p>100).
#
# - M er en liste med en matrix levet med LavTabel
# - min_ er første intervals venstre endepunkt, -infty eller 'ugrupperet'
# - maks er falsk hvis højre endepunkt ikke kendes (kun grupperet)
#
FindFraktil_ := proc(p, M, maks::boolean := true)
    local i_, y_, N_, M_, slope_, min_;
    
    y_ := p;
    N_ := Antal_( M);
    M_ := M[2];
    min_ := FindOption_('start', op(M[1]), '_def' = 'ugrupperet');

    if M_[4, N_] <= y_ and y_ <= 100 then 
        y_ := M_[4, N_];    # Juster for afrundingsfejl
    end if;
    if y_ < 0 or y_ > 100 then
        return _undef;
    elif y_ <= M_[4,1] then
        if min_ = 'ugrupperet' then
            return M_[1, 1];
        elif min_ = -infinity then
            if y_ < M_[4, 1] then
                return -infinity;  # ? _undef
            else
                return M_[1, 1];
            end if;
        else
            slope_ := (M_[1, 1] - min_) / M_[4, 1];
            return min_ + slope_ * y_;
        end if;
    else
        i_ := 1;
        while y_ > M_[4, i_] and i_ <= N_ do
            i_ := i_ + 1;
        end do;
        if min_ = 'ugrupperet' then
            return M_[1, i_];
        elif not maks and i_ = N_ then
            return infinity;
        else
            slope_ := (M_[1, i_] - M_[1, i_ - 1]) / (M_[4, i_] - M_[4, i_ - 1]);
            return M_[1, i_ - 1] + slope_ * (y_ - M_[4, i_ - 1]);
        end if;
    end if;
end proc;


###################
# 
# AntalObservationer(M)
# 
# Returnerer det samlede antal observationer i sættet.
#
AntalObservationer := proc( M )
    local i_, M_;
    
    M_ := FindMatrix_(M);
    add( M_[2, i_], i_ = 1..Antal_(M_));
end proc;


###################
# 
# Optæl_(l)
# 
# - l er en liste eller vektor med observationer
#
# Returnerer en 2xn matrix: 1. række indeholder de værdier, som forekommer 
#   i listen sorteret stigende og anden række indeholder de tilhørende 
#   hyppigheder optalt i listen
#
Optæl_ := proc( l::Or(list, Vector) )
    local tal_, srt_, tab_, i_, l_;

    tal_ := Statistics:-Tally( l);
    srt_ := sort( tal_, (x, y) -> lhs(x) < lhs(y) );
    
    tab_ := Matrix(2, nops(srt_));
    for i_ to nops(srt_) do
        tab_[1, i_] := lhs(srt_[i_]);
        tab_[2, i_] := rhs(srt_[i_]);
    end do;
    tab_;
end proc;


###################
# 
# OptælBin_(l)
# 
# - l er en liste eller vektor med observationer
# - bins er en liste eller vektor med intervalendepunkter (start, ..., slut)
#
# Returnerer en 2xn matrix: 1. række indeholder de værdier, som forekommer 
#   i listen sorteret stigende og anden række indeholder de tilhørende 
#   hyppigheder optalt i listen
#
OptælBin_ := proc( l::Or(list, Vector), bins::list )
    local tab_, i_, l_, N_, idx_, count_, val_;

    l_ := sort( l );
    if type(l, Vector) then
        N_ := op(1, l);
    else
        N_ := nops(l);
    end;
    tab_ := Matrix(2, nops(bins) - 1);
    count_ := proc(val)
        local n_;
        n_ := 0;
        while (idx_ <= N_ and l_[idx_] <= val) do
            ++n_;
            ++idx_;
        end do;
        n_;
    end proc;

    idx_ := 1;
    for i_, val_ in bins[2..-1] do
        tab_[1, i_] := val_;
        tab_[2, i_] := count_(val_);
    end do;
    tab_;
end proc;


use DocumentTools, DocumentTools:-Layout in

###################
# 
# LavTabel(M [, opt, ...])
# 
# - M er en 2xn- (eller nx2)-matrix med hyppigheder eller 
#     en liste med observationer
#   hvis M er en matrix så er tallene i første række (søjle) observationerne
#   (for ugrupperet statistik) eller højre endepunkt for
#   observationsintervallerne (for grupperet statistik)
# - tilvalg: start=, grupperet, observation, vis[=frekvens,kumuleret]
#
# Returnerer liste med startværdi og ny matrix med oprindeligt indhold + 2 
# rækker med hhv. frekvenser og kumulerede frekvenser regnet i procent
#
LavTabel := proc( M::Or(Matrix,list,Vector), intervaller::Or(list,Vector) := NULL )
    local res_, data_, M_, N_, start_, slut_, Table_, vis_, rækker_, l_, tally_, d_, intervaller_;

    if type( M, 'Matrix') then
        M_ := TilpasData_( M);
    else
        if intervaller = NULL and not HasOption_('start', _rest) then  # Ugrupperet
            M_ := Optæl_( M);
        else    # grupperet
            if intervaller = NULL then
                if not HasOption_('slut', _rest) or not HasOption_('n', _rest) then
                    error("Du mangler at angive en slutværdi (slut=...) og/eller antallet af intervaller (n=...)");
                end;
                start_ := FindOption_('start', _rest);
                slut_ := FindOption_('slut', _rest);
                d_ := (slut_ - start_) / FindOption_('n', _rest);
                intervaller_ := evalf([seq(start_..slut_, d_)]);
            else
                intervaller_ := convert(intervaller, 'list');
                if HasOption_('start', _rest) then
                    intervaller_ := [FindOption_('start', _rest), op(intervaller_)]
                end if;
            end if;
            M_ := OptælBin_(M, intervaller_);
        end if;
    end if;

    N_ := Antal_( M_);
    data_ := Matrix(4, N_);
    data_[1] := M_[1];
    data_[2] := M_[2];
    data_[3] := M_[2] / AntalObservationer(M_) * 100.0;
    data_[4] := Statistics:-CumulativeSum(data_[3]);

    if HasOption_(['start', 'grupperet'], _rest) then
        start_ := ['start'=FindOption_('start', _rest, _def=-infinity)];
    else
        start_ := [];
    end if;

    if HasOption_('vis', _rest) then
        rækker_ := LavObservationer_(M_, op(start_), _rest),
            LavRække_("Hyppighed", data_[2], 'cifre'=0);
        vis_ := FindOption_('vis', _rest, '_def'='alt');
        if vis_ = 'frekvens' or vis_ = 'alt' then
            rækker_ ,= LavRække_("Frekvens", data_[3], _rest);
        end if;
        if vis_ = 'kumuleret' or vis_ = 'alt' then
            rækker_ ,= LavRække_("Kumuleret frek.", data_[4], _rest);
        end if;
        Table_ := Table(Column()$(N_ + 1), rækker_);
        InsertContent(Worksheet(Table_));
    end if;
    [start_, data_];
end proc;

###################
# 
# LavInterval_
#
# Laver en tabelcelle med et interval
# - begynd, slut intervallets start- hhv. slutværdi
#       ingen start- hhv slutværdi angives med +-infinity
#
LavInterval_ := proc(begynd, slut, cifre)
    local start_, end_;

    if not type(begynd, 'numeric') then 
        start_ := Equation(-∞);
    else
        start_ := convert(Afrund_(begynd, cifre), 'string');
    end if;
    if not type(slut, 'numeric') then 
        end_ := Equation(∞);
    else
        end_ := convert(Afrund_(slut, cifre), 'string');
    end if;
    Cell(Textfield("[", start_, ";", end_, "]"));
end proc;

###################
# 
# LavObservationer_
#
# Laver en tabelrække med observationer
# - M er en 2xn-matrix 
# - start er startværdien for første interval eller false ved ugruppered data
#
LavObservationer_ := proc(M, { start := false })
    local begin_, end_, row_, cifre_;
    
    cifre_ := FindOption_('cifre', _rest, _def=3);
    
    row_ := FindOption_('observation', _rest, _def="Observation");
    
    if start = false then
        row_ ,= op(convert(M[1], 'list'));
    else
        begin_ := start;
        for end_ in M[1] do
            row_ ,= LavInterval_(begin_, end_, cifre_);
            begin_ := end_;
        end do;
    end if;
    Row(row_);
end proc;

###################
# 
# LavRække_
#
# Laver en tabelrække
# - titel er en tekststreng (rækkens titel)
# - M er en vektor med rækkens data
#
LavRække_ := proc(titel, M)
    local L_, cifre_;

    L_ := convert(M, 'list');
    cifre_ := FindOption_('cifre', _rest, _def=3);
    L_ := map(x->convert(Afrund_(x, cifre_), 'string'), L_);    
    Row(titel, op(L_));
end proc;

end use; # DocumentTools, DocumentTools:-Layout

###################
# 
# Stolpediagram(M [, opt...])
# 
# Tegner stolpediagram for en ikke-grupperet statistik
#
# - M er en liste lavet med LavTabel
# - opt kan være: 'x'=start..stop og 'y'=start..stop
#
Stolpediagram := proc( M )
    local i_, p_, ymaks_, M_, xrange_, yrange_;
    
    p_ := NULL; ymaks_ := 0; M_ := M[2];
    
    for i_ to Antal_(M) do
        p_ := p_, plot([M_[1, i_], t, t = 0..M_[3, i_]]);
        if M_[3, i_] > ymaks_ then ymaks_ := M_[3, i_]; end if;
    end do;
    xrange_, yrange_ := Akser_(args);
    if yrange_ = FAIL then      # TODO: skulle dette være med i Akser_?
        yrange_ := 0..ymaks_ * 1.1;
    end if;
    plots:-display( p_, 'view' = [xrange_, yrange_]);
end proc;


###################
# 
# Prikdiagram(M)
# 
# Tegner stolpediagram for en ikke-grupperet statistik
#
# - M er en liste lavet med LavTabel
#
Prikdiagram := proc(M)
	local data_, xs_, ys_, sizes_, n_, i_, j_;

	n_ := Antal_(M);
	data_ := M[2];
	
	xs_ := NULL; ys_ := NULL;
	for i_ to n_ do
		xs_ ,= seq(data_[1,i_], j_ in 1..data_[2,i_]);
		ys_ ,= seq(j_, j_ in 1..data_[2,i_]);
	end do;
	sizes_ := seq(0.1, j_ in 1..nops([xs_]));
	Statistics:-BubblePlot([xs_], [ys_], [sizes_]); #, 'bubblescale'=0.5);
	#plots:-pointplot([[xs_], [ys_]], view = [0 .. 10, 0 .. 10], symbol = solidcircle, symbolsize = 20)
end proc;


###################
# 
# Trappediagram(M [, opts])
# 
# Tegner trappediagram for ikke-grupperet statistik
#
# - M er en liste lavet med LavTabel
# - opt kan være: 
#   'aflæs'=værdi   - tegn aflæsning ved given observation. Kan gentages
#   'fraktil'=værdi - tegn aflæsning ved given kum. frek.  Kan gentages
#   'kvartiler'     - tegn aflæsning af kvartilsæt.
#   'cifre'=n       - antal betydende cifre som resultater vises med
#
Trappediagram := proc(M)
    local M_, i_, xpts_, ypts_, N_, p_, values_, x_, y_, width_, xrange_, strings_, Linje_;
    
    strings_ := NULL;
    Linje_ := Statistics:-LineChart;
    M_ := M[2];
    
    N_ := Antal_(M_);
    xpts_ := Vector(2 * N_ + 2);
    ypts_ := Vector(2 * N_ + 2);
    xpts_[1] := M_[1, 1] - 0.05 * (M_[1, N_] - M_[1, 1]);
    ypts_[1] := 0;
    ypts_[2] := 0;
    for i_ to N_ do
        xpts_[2 * i_]     := M_[1, i_];
        xpts_[2 * i_ + 1] := M_[1, i_];
        ypts_[2 * i_ + 1] := M_[4, i_];
        ypts_[2 * i_ + 2] := M_[4, i_];
    end do;
    xpts_[2 * N_ + 2] := M_[1, N_] + 0.05 * (M_[1, N_] - M_[1, 1]);
    p_ := Linje_(ypts_, 'xcoords' = xpts_);

    values_ := FindValues_('aflæs', _rest);
    for x_ in values_ do
        if x_ < M_[1, 1] then
            strings_ := strings_, Line_(
                    "Der er ingen observationer på ", x_, " eller derunder."
                );
        elif x_ >= M_[1, N_] then
            strings_ := strings_, Line_(
                    "Alle observationer er ", x_, " eller derunder."
                );
        else
            i_ := 1;
            while i_ < N_ and x_ >= M_[1, i_] do 
                i_ += 1; 
            end do;
            i_ := i_ - 1; y_ := M_[4, i_];
            p_ := p_, Linje_(<0, y_, y_>, 'xcoords' = <x_, x_, 0>);
            strings_ := strings_, Line_(
                    round(y_), "% af observationerne er ", x_, " eller derunder."
                );
        end if;
    end do;

    values_ := FindValues_('fraktil', _rest);
    for y_ in values_ do
        x_ := FindFraktil_(y_, M);
        if x_ = _undef then
            Err_("Kan ikke finde %1 procent-fraktilen. ",
                    "Brug kun tal imellem 0 og 100.", y_
                );
        else
            p_ := p_, Linje_(<0, y_, y_>, 'xcoords' = <x_, x_, 0>);
            strings_ := strings_, Line_(
                    y_, "% fraktilen for observationssættet er ", Afrund_(x_, _rest)
                );
        end if;
    end do;

    if HasOption_('kvartiler', _rest) then
    # Nedre kvartil
    x_ := FindFraktil_(25, M);
    p_ := p_, Linje_(<0, 25, 25>, 'xcoords' = <x_, x_, 0>);
    strings_ := strings_, Line_(
        "Nedre kvartil er ", Afrund_(x_, _rest)
    );

    # Median
    x_ := FindFraktil_(50, M);
    p_ := p_, Linje_(<0, 50, 50>, 'xcoords' = <x_, x_, 0>);
    strings_ := strings_, Line_(
        "Medianen er ", Afrund_(x_, _rest)
    );

    # Øvre kvartil
    x_ := FindFraktil_(75, M);
    p_ := p_, Linje_(<0, 75, 75>, 'xcoords' = <x_, x_, 0>);
    strings_ := strings_, Line_(
        "Øvre kvartil er ", Afrund_(x_, _rest)
    );
    end if;

    # TODO: option x=min..max
    width_ := xpts_[-1] - xpts_[1];
    xrange_ := (xpts_[1] - 0.1 * width_)..(xpts_[-1] + 0.1 * width_);
    p_ := plots:-display(
        [p_], 'view' = [xrange_, 0..100], 'axes' = 'normal'
    );
    Plot_(p_, strings_);
end proc;


###################
# 
# Histogram(M [, opts])
# 
# Tegner et histogram for en grupperet statistik
#
# - M er en liste lavet med LavTabel
# - opts kan være:
#       'start'=værdi
#
Histogram := proc( M )
    local M_, i_, xpts_, ypts_, N_, højde_, max_højde_, min_bredde_, 
            _p, _x1, _x2, _y1, _y2, _min;
    M_ := M[2];
    N_ := Antal_( M);
    _min := FindOption_('start', op(M[1]), '_def' = -infinity);

    if _min = -infinity or M_[1, N_] = infinity then
        error(
            cat(
                "Du kan ikke lave et histogram;\n",
                "Du mangler at angive starten på første interval ",
                "eller slutningen på sidste interval"
            )
        );
    end if;
    højde_ := Vector(N_);
    højde_[1] := M_[3 ,1] / (M_[1, 1] - _min);
    max_højde_ := højde_[1];
    min_bredde_ := (M_[1, 1] - _min);
    for i_ from 2 to N_ do
        højde_[i_] := M_[3, i_] / (M_[1, i_] - M_[1, i_ - 1]);
        if højde_[i_] > max_højde_ then
            max_højde_ := højde_[i_];
        end if;
        if M_[1, i_] - M_[1, i_ - 1] < min_bredde_ then
            min_bredde_ := M_[1, i_] - M_[1, i_ - 1];
        end if;
    end do;
    xpts_ := Vector(3 * N_ + 1);
    ypts_ := Vector(3 * N_ + 1);
    xpts_[1] := _min;
    ypts_[1] := 0;
    for i_ to N_ do
        xpts_[3 * i_ - 1] := xpts_[3 * i_ - 2];
        ypts_[3 * i_ - 1] := højde_[i_];
        xpts_[3 * i_]     := M_[1, i_];
        ypts_[3 * i_]     := højde_[i_];
        xpts_[3 * i_ + 1] := M_[1, i_];
        ypts_[3 * i_ + 1] := 0;
    end do;
    _p := Statistics:-LineChart(ypts_, 'xcoords' = xpts_);
    _x1 := M_[1, N_] - 1.15 * min_bredde_;
    _x2 := _x1 + min_bredde_;
    _y1 := max_højde_ * 1.15;
    _y2 := _y1 + 10 / min_bredde_;
    xpts_ := [_x1, _x2, _x2, _x1, _x1];
    ypts_ := [_y1, _y1, _y2, _y2, _y1];
    _p := _p, Statistics:-LineChart( ypts_, 'xcoords' = xpts_);
    _p := _p, plots:-textplot([(_x1 + _x2)/2, (_y1 + _y2)/2, "10%"]);
    plots:-display(_p, 'axes' = 'normal', axis[2] = ['color' = "White"]);
end proc;

###################
# 
# Sumkurve(M [, opts])
# 
# Tegner sumkurve over en grupperet statistik
#
# - M er en liste lavet med LavTabel. Først række indeholder
#       intervallernes højre endepunkter. Hvis det sidste endepunkt ikke
#       kendes, så bruges infty
# - opts kan være:
#   'aflæs'=værdi   - tegn aflæsning ved given observation. Kan gentages
#   'fraktil'=værdi - tegn aflæsning ved given kum. frek.  Kan gentages
#   'kvartiler'     - tegn aflæsning af kvartilsæt.
#   'cifre'=n       - antal betydende cifre som resultater vises med
#
Sumkurve := proc(M)
    local i_, xpts_, ypts_, N_, min_, antal_, p_, width_, xrange_, values_, x_, y_, 
            str_, harMaks_, start_, strings_, Linje_, hældning_, M_;
    Linje_ := Statistics:-LineChart;
    N_ := Antal_( M);
    M_ := M[2];
    
    antal_ := N_;
    if M_[1, N_] = infinity then
        antal_ := antal_ - 2;
        harMaks_ := false;
    else
        harMaks_ := true;
    end if;
    min_ := FindOption_('start', op(M[1]), '_def' = -infinity);
    if min_ = -infinity then
        antal_ := antal_ - 1;
        start_ := 0;
    else
        start_ := 1;
    end if;
    xpts_ := Vector(antal_ + 2);
    ypts_ := Vector(antal_ + 2);
    if not min_ = -infinity then
        xpts_[1] := min_;
        ypts_[1] := 0;
    end if;
    for i_ to N_-1 do
        xpts_[i_ + start_] := M_[1, i_];
        ypts_[i_ + start_] := M_[4, i_];
    end do;
    if harMaks_ then
        xpts_[antal_ + 1] := M_[1, N_];
        ypts_[antal_ + 1] := M_[4, N_];
        xpts_[antal_ + 2] := M_[1, N_] + 0.1*(M_[1, N_] - M_[1, 1]);
        ypts_[antal_ + 2] := M_[4, N_];
    end if;
    p_ := Linje_( ypts_, 'xcoords' = xpts_);

    strings_ := NULL;
    values_ := FindValues_('aflæs', _rest);
    
    # Fejlmeddelelser
    str_ := [        
        "Kan ikke finde %a%%-fraktilen. ",
        "Kan ikke finde %s.\n  ",
        cat("Den ligger i det %s observationsinterval, ",
            "og du har ikke angivet hvor det %s"),
        "Kan ikke aflæse ved %a. "
    ];
    for x_ in values_ do
        if x_ <= M_[1, 1] then
            if x_ = M_[1, 1] then
                strings_ := strings_, Line_(
                        round(M_[4, 1]), "% af observationerne ligger under ", x_
                    );
            elif min_ = -infinity then
                error sprintf(
                    cat(str_[4], str_[3]), x_, "første", "starter"
                );
            elif x_ < min_ then
                strings_ := strings_, Line_(
                        "Der er ingen observationer under ", x_
                    );
            else
                y_ := M_[4, 1]*(M_[1, 1] - x_)/(M_[1, 1] - min_);
                p_ := p_, Linje_(<0, y_, y_>, 'xcoords' = <x_, x_, 0>);
                strings_ := strings_, Line_(
                        round(y_), "% af observationerne ligger under ", x_
                    );
            end if;
        elif M_[1, N_] = infinity then
            if x_ > M_[1, N_-1] then
                error sprintf(
                        cat(str_[4], str_[3]), x_, "sidste", "slutter"
                    ):
            end if;
        elif x_ > M_[1,N_] then
            strings_ := strings_, Line_(
                    "Alle observationer ligger under ", x_
                );
        else
            i_ := 1;
            while i_ < N_ and x_ > M_[1, i_] do 
                i_ += 1; 
            end do;
            hældning_ := (M_[4, i_] - M_[4, i_-1]) / (M_[1, i_] - M_[1, i_-1]);
            y_ := M_[4, i_ - 1] + hældning_ * (x_ - M_[1, i_ - 1]);
            p_ := p_, Linje_(<0, y_, y_>, 'xcoords' = <x_, x_, 0>);
            strings_ := strings_, Line_(
                    round(y_), "% af observationerne ligger under ", x_
                );
        end if;
    end do;

    values_ := FindValues_('fraktil', _rest);
    for y_ in values_ do
        x_ := FindFraktil_(y_, M, harMaks_);
        if x_ = _undef then
            error sprintf(
                    cat("Kan ikke finde %a%%-fraktilen. ",
                        "Brug kun tal imellem 0 og 100.\n"),
                    y_
                );
        elif x_ = -infinity then
            error sprintf(cat(str_[1], str_[3]), y_, "første", "starter");
        elif x_ = infinity then # fraktil i_ sidste interval
            error sprintf(cat(str_[1], str_[3]), y_, "sidste", "slutter");
        else
            p_ := p_, Linje_(<0, y_, y_>, 'xcoords' = <x_, x_, 0>);
            strings_ := strings_, Line_(
                    y_, "% fraktilen for observationssættet er ", Afrund_(x_, _rest)
                );
        end if;
    end do;

    if HasOption_('kvartiler', _rest) then

        # Nedre kvartil
        x_ := FindFraktil_(25, M, harMaks_);
        if x_ = infinity then # nedre kvartil i_ sidste interval
            error sprintf(cat(str_[2], str_[3]), 
                    "den nedre kvartil", "sidste", "slutter"
                );
        elif x_ = -infinity then # nedre kvartil i_ første interval
            error sprintf(cat(str_[2], str_[3]), 
                    "den nedre kvartil", "første", "starter"
                );
        else
            p_ := p_, Linje_(<0, 25, 25>, 'xcoords' = <x_, x_, 0>);
            strings_ := strings_, Line_(
                    "Nedre kvartil er ", Afrund_(x_, _rest)
                );
        end if;

        # Median
        x_ := FindFraktil_(50, M, harMaks_);
        if x_ = infinity then
            error sprintf(cat(str_[2], str_[3]), 
                    "medianen", "sidste", "slutter"
                );
        elif x_ = -infinity then
            error sprintf(cat(str_[2], str_[3]), 
                    "medianen", "første", "starter"
                );
        else
            p_ := p_, Linje_(<0, 50, 50>, 'xcoords' = <x_, x_, 0>);
            strings_ := strings_, Line_(
                    "Medianen er ", Afrund_(x_, _rest)
                );
        end if;

        # Øvre kvartil
        x_ := FindFraktil_(75, M, harMaks_);
        if x_ = infinity then
            error sprintf(cat(str_[2], str_[3]), 
                    "den øvre kvartil", "sidste", "slutter"
                );
        elif x_ = -infinity then
            error sprintf(cat(str_[2], str_[3]), 
                    "den øvre kvartil", "første", "starter"
                );
        else
            p_ := p_, Linje_(<0, 75, 75>, 'xcoords' = <x_, x_, 0>);
            strings_ := strings_, Line_(
                    "Øvre kvartil er ", Afrund_(x_, _rest)
                );
        end if;
    end if;

    width_ := xpts_[antal_ + 2] - xpts_[1];
    xrange_ := (xpts_[1] - 0.1 * width_)..(xpts_[antal_+2] + 0.1 * width_);
    p_ := plots:-display([p_], 'view' = [xrange_, 0..100], 'axes' = 'normal');
    Plot_(p_, strings_);
end proc;

###################
# 
# Fraktil(M, p [, opts])
# 
# Returnerer p%-fraktilen for statistikken i matricen M lavet med LavTabel
#
# - p er et tal imellem 0 og 100
# - opts kan være:
#   'cifre' = antal  - angiv antal cifre, som resultater skal vises med
#
Fraktil := proc( M, p)
    local i_, N_, a_, x_, maks_;

    N_ := Antal_( M);
    if M[2][1, N_] = infinity then
        maks_ := false;
    else
        maks_ := true;
    end if;
    x_ := FindFraktil_(p, M, maks_);
    if x_ = _undef then
        error("Udefineret fraktil! Brug kun værdier imellem 0 og 100");
    elif x_ = -infinity then
        error sprintf(
                cat("%a_%%-fraktilen ligger i_ første interval, ",
                    "og du har ikke angivet dets startpunkt"), 
                p
            );
    elif x_ = infinity then
        error sprintf(
                cat("%a_%%-fraktilen ligger i_ sidste interval, ",
                    "og du har ikke angivet dets slutpunkt"), 
                p
            );
    end if;
    Afrund_(x_, _rest);
end proc;


###################
# 
# Kvartilsæt(M [,opts])
# 
# Returnerer kvartilsættet for statistikken i matricen M
#
# - M er en liste lavet med LavTabel.
# - opts - se Fraktil
#
Kvartilsæt := proc( M)
    Fraktil(M, 25, _rest), Fraktil(M, 50, _rest), Fraktil(M, 75, _rest);
end proc;


###################
# 
# GetMinimum_
# 
# Finder minimumsværdien for en grupperet statistik i en liste af options
# Afbryder med feljmeddelelse, hvis start=<værdi> mangler
#
GetMinimum_ := proc(M)
    local min_;

    if not HasOption_('start', M[1]) then
        min_ := M[2][1, 1];
    else
        min_ := FindOption_('start', op(M[1]), '_def' = -infinity);
    end if;
    if min_ = -infinity then
        error("Du har grupperede data uden en minimumsværdi");
    end if;
    min_;
end proc;


###################
# 
# Boksplot(M, ... [, opts])
# 
# Tegner et eller flere boksplot i samme diagram
#
# - M, ... er et eller flere input, som kan være
#       en 4xn-matrix lavet med LavTabel
#       en liste eller Vector med 5 værdier (min, kvartilsæt, max)
#       en liste med observationer
#
# - hvis opts indeholder 'start'=værdi, så bruges samme startværdi for alle
#   for alle data-sæt, som ikke har egen startværdi 
#
# Man kan blande forskellige typer argumenter i samme funktionskald
#
Boksplot := proc( M::seq(Or(Matrix,Vector,list)) )
    local p_, m_, opts_, min_;
    
    opts_ := 'orientation'='horizontal', 'deciles'=false, 
            'mean'=false, axis[2]=['color'='white'];
    p_ := NULL;
    for m_ in [M] do
        if type(m_, 'list') and nops(m_) = 2 and type(m_[2], 'Matrix') then
          min_ := GetMinimum_(m_);
          p_ := [min_, Kvartilsæt(m_, _rest), m_[2][1,Antal_(m_)]], p_;
      else
          p_ := m_, p_; # list - observationssæt
      end if;
    end do;
    Statistics:-BoxPlot([p_], opts_);
end proc;
Boxplot := Boksplot;

###################
# 
# Middelværdi(M [, opts])
# 
# Beregner middelværdien for en grupperet eller ikke-grupperet statistik
#
# - M er en 4 x n-matrix lavet med LavTabel
# - opts kan være:
#       'cifre'=n   hvor n er det ønskede antal betydende cifre
#
# Det er en fejl at angive 'grupperet' uden en startværdi i options.
# Statistikken antages at være ikke-grupperet med mindre andet angives.
#
Middelværdi := proc( M::list )
    local res_, i_, min_, M_;
    
    M_ := M[2];
    if HasOption_('start', M[1]) then
        min_ := GetMinimum_(M); # !!!
        res_ := 1/2 * (M_[1, 1] + min_) * M_[3, 1];
        for i_ from 2 to Antal_(M) do
            res_ := res_ + 1/2 * (M_[1, i_] + M_[1, i_-1]) * M_[3, i_];
        end do;
    else
        res_ := add( M_[1, i_] * M_[3, i_], i_ = 1..Antal_(M));
    end if;
    Afrund_(res_/100, _rest);
end proc;


###################
# 
# Varians_(M)
# 
# Beregner variansen for en grupperet eller ikke-grupperet statistik
#
# - M er en 4 x n-matrix lavet med LavTabel
#
# Det er en fejl at angive 'grupperet' uden en startværdi i options.
# Statistikken antages at være ikke-grupperet med mindre andet angives.
#
Varians_ := proc( M::list )
    local m_, res_, i_, min_, M_;

    m_ := Middelværdi(M);
    M_ := M[2];
    if HasOption_('start', M[1]) then
        min_ := GetMinimum_(M);
        res_ := (1/2 * (M_[1, 1] + min_) - m_)^2 * M_[3, 1];
        for i_ from 2 to Antal_(M) do
            res_ := res_ + (1/2 * (M_[1, i_] + M_[1, i_-1]) - m_)^2 * M_[3, i_];
        end do;
    else
        res_ := add( (M_[1, i_] - m_)^2 * M_[3, i_], i_ = 1..Antal_(M));
    end if;
    res_/100;
end proc;

###################
# 
# Varians(M [, opts])
# 
# Beregner variansen for en grupperet eller ikke-grupperet statistik
#
# - M er en liste lavet med LavTabel
# - opts kan være:
#       'cifre'=n   hvor n er det ønskede antal betydende cifre
#
# Det er en fejl at angive 'grupperet' uden en startværdi i options.
# Statistikken antages at være ikke-grupperet med mindre andet angives.
#
Varians := proc( M::list )
    Afrund_(Varians_(M), _rest);
end proc;

###################
# 
# Standardafvigelse(M [, opts])
# 
# Beregner stan.afv. for en grupperet eller ikke-grupperet statistik
#
# - M er en liste lavet med LavTabel
# - opts kan være:
#       'cifre'=n   hvor n er det ønskede antal betydende cifre
#
# Det er en fejl at angive 'grupperet' uden en startværdi i options.
# Statistikken antages at være ikke-grupperet med mindre andet angives.
#
Standardafvigelse := proc( M::list )
    Afrund_(sqrt(Varians_(M)), _rest);
end proc;
Spredning := Standardafvigelse;

###################
# 
# Typeinterval(M)
# 
# Finder typeintervallet (-erne) for en grupperet statistik
#
# - M er en liste lavet med LavTabel
#
Typeinterval := proc( M::list )
    local prev_, maks_, i_, hjd_, res_, M_;

    M_ := M[2];
    prev_ := GetMinimum_(M);
    maks_ := 0;
    for i_ to Antal_(M) do
        hjd_ := M_[2, i_] / (M_[1, i_] - prev_);
        if hjd_ = maks_ then
            res_ := res_, prev_..M_[1, i_];
        elif hjd_ > maks_ then
            res_ := prev_..M_[1, i_];
            maks_ := hjd_;
        end if;
        prev_ := M_[1, i_];
    end do;
    [res_]; # TODO: Udskriv intervaller korrekt
end proc;


###################
# 
# Typetal(M)
# 
# Finder typetallet (-erne) for en ugrupperet statistik
#
# - M er en liste lavet med LavTabel
#
Typetal := proc ( M::list )
    local maks_, i_, res_, M_;

    M_ := M[2];
    maks_ := 0;
    for i_ to Antal_(M) do
        if M_[2, i_] = maks_ then
            res_ := res_, M_[1, i_];
        elif M_[2, i_] > maks_ then
            res_ := M_[1, i_];
            maks_ := M_[2, i_];
        end if;
    end do;
    [res_];
end proc;


###################
# 
# Deskriptorer(M  [, opts])
# 
# Udskriver deskriptorer for en statistik
#
# - M er en liste lavet med LavTabel
# - opts kan være:
#       'tekst'         hvis resultaterne ønskes udskrevet
#       'størrelse'
#       'typeinterval' eller 'typeint'
#       'typetal'
#       'middelværdi' eller 'middelv'
#       'standardafvigelse' eller 'stdafv'
#       'varians'
#       'grupperet'
#
# Returnerer en liste med de fundne værdier
#
Deskriptorer := proc( M::list )
    local opts_, opt_, min_, all_, res_, resultat_, m_, i_, text_, Str_, 
            str_, strings_;
    all_ := [
        'typeinterval', 'typeint', 'typetal', 'middelv', 'middelværdi', 
        'stdafv', 'standardafvigelse', 'varians'
    ];
    text_ := FindOption_('tekst', _rest, '_def' = true);
    resultat_ := NULL;
    strings_ := NULL;
    opts_ := FindOptions_(all_, _rest);
    if opts_ = [] then
        if HasOption_('start', M[1]) then
            opts_ := [
                'størrelse', 'typeint', 'middelv', 'stdafv', 'varians'
            ];
        else
            opts_ := [
                'størrelse', 'typetal', 'middelv', 'stdafv', 'varians'
            ];
        end if;
    end if;
    for opt_ in opts_ do
        if opt_ = 'typeint' or opt_ = 'typeinterval' then
            res_ := Typeinterval(M);
            if text_ then
                if nops(res_) > 1 then 
                    Str_ := "rne"; else Str_ := "t"; 
                end if;
                str_ := "Typeintervalle", Str_, " er: ]", 
                        op(1,res_[1]), "; ", op(2,res_[1]), "]";
                if nops(res_) > 1 then
                    for i_ in res_[2..-2] do
                        str_ := str_, ", ]", op(1, i_), "; ", op(2, i_), "]";
                    end do;
                    str_ := str_, " og ]", op(1, res_[-1]), "; ", op(2, res_[-1]), "]";
                end if;
                strings_ := strings_, Line_(str_);
            end if;
        elif opt_ = 'typetal' then
            res_ := Typetal(M);
            if text_ then
                if nops(res_) > 1 then 
                    Str_ := "ne"; 
                else 
                    Str_ := "t"; 
                end if;
                str_ := "Typetalle", Str_, " er: ", res_[1];
                if nops(res_) > 1 then
                    for i_ in res_[2..-2] do
                        str_ := str_, ", ", i_;
                    end do;
                    str_ := str_, " og ", res_[-1];
                end if;
                strings_ := strings_, Line_(str_);
            end if;
        elif opt_ = 'størrelse' then
            res_ := AntalObservationer(M);
            if text_ then
                strings_ := strings_, Line_(
                        "Der er ", res_, " observationer i sættet"
                    );
            end if;
        elif opt_ = 'middelv' or opt_ = 'middelværdi' then
            res_ := Middelværdi(M); # TODO args
            if text_ then
                strings_ := strings_, Line_(
                        "Middelværdien er ", Afrund_(res_, _rest)
                    );
            end if;
        elif opt_ = 'stdafv' or opt_ = 'standdardafvigelse' then
            res_ := Standardafvigelse(M);
            if text_ then
                strings_ := strings_, Line_(
                        "Standardafvigelsen er ", Afrund_(res_, _rest)
                    );
            end if;
        elif opt_ = 'varians' then
            res_ := Varians(M);
            if text_ then
                strings_ := strings_, Line_(
                        "Variansen er ", Afrund_(res_, _rest)
                    );
            end if;
        end if;
        resultat_ := resultat_, res_;
    end do;
    if not strings_ = NULL then
        Print_(strings_);
    end if;
    resultat_;
end proc;
