#############################################################################
#                                                                           #
# Copyright (C) 2018 by Claus Christensen                                   #
# This file is part of KGModul                                              #
#                                                                           #
# This program is free software: you can redistribute it and/or modify      #
# it under the terms of the GNU General Public License as published by      #
# the Free Software Foundation, either version 3 of the License, or         #
# (at your option) any later version.                                       #
#                                                                           #
# This program is distributed in the hope that it will be useful,           #
# but WITHOUT ANY WARRANTY; without even the implied warranty of            #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
# GNU General Public License for more details.                              #
#                                                                           #
# You should have received a copy of the GNU General Public License         #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    #
#                                                                           #
#############################################################################

###################
# 
# GetOption_
#
# find værdien knyttet til optionen opt. Kun sidste forekomst returneres
#
# GetOption_( opt, seq ) eller
# GetOption_( opt, arg2, arg3, ... )
#
#    opt er navnet på en option
#    seq er en liste eller en mængde
#    arg1, arg2, ... er en vilkårlig sekvens af argumenter
#
# returnerer værdien knyttet til opt samt resten af argumenterne som liste
#
# hvis arg ikke forekommer i listen eller hvis den sidste forekomst ikke 
# har en værdi, så returneres FAIL, 
# medmindre der er en option '_def'=værdi; i så fald bruges værdi.
#
# NB: Det er vigtigt at navnet på en option ikke også bruges som 
#   variabelnavn i den procedure som kalder GetOption_!
#
GetOption_ := proc(opt, {_def := FAIL})
    local arg_, val_, rest_, tilvalg_;
    rest_ := NULL;
    val_ := FAIL;        # returneres, medmindre en værdi findes
    
    if nops([_rest]) = 1 and type(_rest, Or('list','set')) then
        # sekvens af optioner givet som en liste eller mængde
        tilvalg_ := _rest;
    else
        # sekvens af optioner givet som argumentliste
        tilvalg_ := [_rest];
    end if;
    
    # gå igennem sekvensen af options og søg efter den ønskede option
    # gennemgå hele listen, sådan at opt ikke optræder i rest_-listen
    for arg_ in tilvalg_ do
        # søg efter option af formen navn=val hvor navn er det ønskede
        if type(arg_, `=`) and lhs(arg_) = opt then
            val_ := rhs(arg_);
        elif not arg_ = opt then     
            rest_ ,= arg_;
        else
            # fandt den søgte option, men uden værdi
            val_ := FAIL;
        end if;
    end do;

    if val_ = FAIL then
        val_ := _def;
    end if;
    
    val_, [rest_];
end proc;

    
###################
# 
# FindOption_
#
# Som GetOption_, men returnerer kun værdien knyttet til opt
#
# NB: Det er vigtigt at navnet på en option ikke også bruges som 
#   variabelnavn i den procedure som kalder FindOption_!
#
FindOption_ := proc()
    local _find, _other;

    _find, _other := GetOption_(args);
    _find;
end proc;

    
    
###################
# 
# GetOptions_(opt, args)
# 
# Finder alle forekomster af en option
#
# opt er en option
# args er et argument eller en liste eller sekvens af argumenter
# 
# returnerer lister med de fundne værdier og med resten af argumenterne
#
GetOptions_ := proc(opt, {_def := FAIL})
    local _arg, _val, _other, _tilvalg;
    _other := [];
    _val := []; 

    if nops([_rest]) = 1 and type(_rest, Or('list','set')) then
        _tilvalg := _rest;
    else
        _tilvalg := [_rest];
    end if;

    for _arg in _tilvalg do
        if type(_arg, `=`) and lhs(_arg) = opt then
            _val := [op(_val), rhs(_arg)];
        elif not _arg = opt then
            _other := [op(_other), _arg];
        end if;
    end do;

    # Hvis option ej defineret men har standardværdi
    if _val = [] and not _def = FAIL then         
        _val := [_def];
    end if;
    _val, _other;
end proc;

    

###################
# 
# HasOption_(Opts, seq)
#
# Forekommer et af navnene i Opts som en option?
#
# HasOption_( Opts, seq )
# HasOption_( Opts, arg2, arg3, ... )
#
#    Opts er et navn eller en liste af navne
#    seq er er liste eller mængde
#
#    returnerer 'true', hvis et 'navn' i Opts findes blandt elementerne i 
#       seq eller blandt argumenterne arg2, arg3, ... 
#       enten som 'navn' eller som 'navn = ...'
#    ellers returneres false
#
# NB: Det er vigtigt at navnet på en option ikke også bruges som 
#     variabelnavn i den procedure som, bruger HasOption_!
#
HasOption_ := proc(Opts)
    local _liste, _element, _opts;
    _opts := Opts;
    
    # sørg for at _opts er en liste eller mængde
    if  not type(_opts, 'sequential') then 
        _opts := [_opts];
    end if;
    
    # sørg for at 'liste' er en liste
    if nargs = 2 and type(args[2], 'sequential') then 
        _liste := args[2];
    else
        _liste := [args[2..-1]];
    end if;
    
    for _element in _liste do
        # undersøg kun venstre side af ligheder
        if type(_element, `=`) then 
            _element := lhs(_element);
        end if;
        
        # er det en af de ønskede optioner?
        if _element in _opts then 
            return true;
        end if;
    end do;
    
    # hvis vi kommer hertil, så fandt vi ikke et navn i Opts
    return false;
end proc;

###################
# 
# FilterOption_(opt, args)
# 
# Returnerer 'true' og en filtreret liste af argumenter, hvis opt forekommer som
# option blandt argumenterne; ellers false og en liste med de oprindelige argumenter
#
# Elementer af formen opt og opt=... vil være fjernet fra argumentlisten
#
# - opt er navnet på en option
# - args er en sekvens eller liste af 0 eller flere argumenter
#    
# NB: Det er vigtigt at navnet på en option ikke også bruges som 
# variabelnavn i den procedure som, bruger FilterOption_!
#
FilterOption_ := proc(opt)
    local _val, _rst;
    
    if HasOption_(opt, _rest) or HasOption_('_def', _rest) then
        _val, _rst := GetOption_(opt, _rest);
        'true', _rst;
    else
        false, [_rest];
    end if;
end proc;


###################
# 
# FilterOptions_(opts, args)
# 
# Returnerer en filtreret liste af argumenter, hvor optionerne i opts er 
# filtreret fra. 
#
# Elementer af formen opt og opt=... vil være fjernet fra argumentlisten
#
# - opts er en liste med navne på options
# - args er en sekvens eller liste af 0 eller flere argumenter
#    
# NB: Det er vigtigt at navnet på en option ikke også bruges som 
# variabelnavn i den procedure som, bruger FilterOptions_!
#
FilterOptions_ := proc(opts)
    local _rst, _bool, _opt;
    
    _rst := [_rest];
    for _opt in opts do
        _bool, _rst := FilterOption_(_opt, op(_rst));
    end do;
    _rst;
end proc;



###################
# 
# GetType_(type, args)
# 
# Finder alle værdier af en bestemt type
#
# type er den ønskede type
# args er en liste eller sekvens af argumenter
#
# returnerer lister med de fundne argumenter og resten af argumenterne
#
GetType_ := proc( whatType )
    local _argument, _elt, _isType, _notType;
    _isType := []; _notType := [];

    if nargs = 2 and type(args[2], Or('list','set')) then
        _argument := args[2];
    else
        _argument := [args[2..-1]];
    end if;
    for _elt in _argument do
        if type(_elt, whatType) then
            # eval er nødvendig aht. anonyme funktioner
            _isType := [op(_isType), eval(_elt)];   
        else
            _notType := [op(_notType), eval(_elt)];
        end if;
    end do;
    _isType, _notType;
end proc:
    
    

###################
# 
# HasType_(types, args)
# 
# Returnerer 'true', hvis en 'type' i types findes blandt elementerne i 
# args, enten som 'navn' eller som 'navn = ...'; ellers returneres 'false'
#
# - types er en type eller liste af typer
# - arsg er en sekvens eller liste af 0 eller flere argumenter
#    
# NB: Det er vigtigt at navnet på en option ikke også bruges som 
# variabelnavn i den procedure som, bruger HasOption_!
#
HasType_ := proc(Types)
    local _liste, _element, _types, _type;
    _types := Types;
    
    # sørg for at _opts er en _liste eller mængde
    if  not type(_types,'sequential') then 
        _types := [_types];
    end if;
    
    # sørg for at '_liste' er en _liste
    if nargs=2 and type(args[2], 'sequential') then 
        _liste := args[2];
    else
        _liste := [_rest];
    end if;
    
    for _element in _liste do
        if type(_element,`=`) then # undersøg kun venstre side af ligheder
            _element := lhs(_element);
        end if;
        for _type in _types do
            if type(_element, _type) then # er det en af de ønskede typer?
                return true;
            end if;
        end do;
    end do;
    return false;
end proc;


###################
#
# GetVectors_(args)
#
# Returnerer en liste med de af argumenterne som er vektor-udtryk,
# vektor-funktioner (evt. med tilhørende parameterinterval) og en med resten 
# af argumenterne
#
GetVectors_ := proc()
    local _vectors, _rst, _items, _item;
    
    # vektor-udtryk
    _vectors, _rst := GetType_('Vector', args);
    _vectors := op(_vectors);
    # vektor-funktioner
    _items, _rst := GetType_('procedure', _rst);
    _rst := op(_rst);

    for _item in _items do
        if type(_item(_t), 'Vector') then
            _vectors ,= eval(_item);
        else
            _rst ,= eval(_item);
        end if;
    end do;

    # par af vektor-parameterinterval som liste
    _items, _rst := GetType_('list', [_rst]);
    _rst := op(_rst);
    for _item in _items do
        if nops(_item) = 2 and type(_item[2],`..`) then
            if type(_item[1], 'Vector') then
                _vectors ,= _item;
            elif type(_item[1], 'procedure') and type(_item[1](_t), 'Vector') then
                _vectors ,= _item;
            else
                _rst ,= _item;
            end if;
        else
            _rst ,= _item;
        end if;
    end do;
    
    [_vectors], [_rst];
end proc;

    
###################
#
# FindValues_(opt, rest)
#
# Finder værdierne knyttet til opt i argumentlisten
# Returnerer (evt tom) liste med de fundne værdier
#
FindValues_ := proc(opt)
    local values, liste, element;
    values := NULL;
    if nargs=2 and type(args[2], 'sequential') then
        liste := args[2];
    else
        liste := [_rest];
    end if;

    for element in liste do
        if type(element, `=`) and lhs(element) = opt then
            values := values, rhs(element);
        end if;
    end do;
    [values];
end proc;

###################
# 
# FindOptions_(opts, args)
# 
# Find de af argumenterne, som har en af de ønskede options.
#
# - opts er et navn eller en liste med navne
# - args er en sekvens eller en list eller mængde med argumenter
#
# NB: Det er vigtigt at navnet på en option ikke også bruges som 
# variabelnavn i den procedure som, bruger FindOptions_!
#
# Returnerer en liste med de options, som forekommer i opts
#
FindOptions_ := proc(optionS)
    local opts, liste, element, _opts;

    if  not type(optionS, 'sequential') then
        _opts := [optionS];
    else
        _opts := optionS;
    end if;
    opts := [];

    if nargs=2 and type(args[2], 'sequential') then
        liste := args[2];
    else
        liste := [args[2..-1]];
    end if;

    for element in liste do
        if type(element, `=`) then
            element := lhs(element);
        end if;
        if element in _opts then
            opts := [op(opts), element];
        end if;
    end do;
    return opts;
end proc;


