#############################################################################
#                                                                           #
# Copyright (C) 2018-20 by Claus Christensen                                #
# This file is part of KGModul                                              #
#                                                                           #
# This program is free software: you can redistribute it and/or modify      #
# it under the terms of the GNU General Public License as published by      #
# the Free Software Foundation, either version 3 of the License, or         #
# (at your option) any later version.                                       #
#                                                                           #
# This program is distributed in the hope that it will be useful,           #
# but WITHOUT ANY WARRANTY; without even the implied warranty of            #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
# GNU General Public License for more details.                              #
#                                                                           #
# You should have received a copy of the GNU General Public License         #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    #
#                                                                           #
#############################################################################

###################
# 
# PrintFunk_(e, t, f [, 'ligning'] [, 'potens'] [, 'cifre' = n] [, rkvadrat])
# 
# Skriver resultatet af en regressionsanalyse. Bruges af Model_
#
# - e er udtryk i x
# - t symbolet for den uafhængige variabel
# - f symbolet for afhængig variabel
# - n er antal betydende cifre
# - 'ligning' angiver at venstre side blot skal skrives som en variabel,
#     ellers bruges alm. funktionsnotation.
# - 'potens' angiver, at der er tale om en potenssammenhæng.
#     Pga. en ejendommelighed i Maples afrundingsalgoritme bliver 
#     eksponenter ikke afrundet. Dette kræver derfor særlig håndtering.
# - 'rkvadrat' angiver, at r-i-anden også skal udskrives
#
PrintFunk_ := proc( e::algebraic, t::symbol, f::symbol, model)
    local _eksp, _a, _b, lines_, arr_;
    _eksp := e;
    if HasOption_('potens', _rest) then
        _b := op(1, e);
        _a := Afrund_(op(2, op(2, e)), _rest);
        _eksp := _b*t^_a;
    end if;
    if HasOption_('ligning', _rest) then
        lines_ := Line_(f, " = ", Afrund_(eval(_eksp), _rest) );
    else
        lines_ := Line_(f, "(", t, ") = ", Afrund_(eval(_eksp), _rest) );
    end if;
    if HasOption_('rkvadrat', _rest) then
        lines_ ,= Line_(R^2, " = ", Afrund_(model['rkvadrat'], _rest) );
    end if;
    if HasOption_('a', _rest) then
        lines_ ,= Line_('a', " = ", Afrund_(model['a'], _rest) );
    end if;
    if HasOption_(['konfidensinterval_a', 'konfint_a'], _rest) then
        lines_ ,= Line_("konfidensinterval for a : ", Interval_(model['konfint_a'], _rest) );
    end if;
    if HasOption_(['standardafvigelse_a', 'stdafv_a'], _rest) then
        lines_ ,= Line_("standardafvigelse for a : ", Afrund_(model['stdafv_a'], _rest) );
    end if;
    if HasOption_('b', _rest) then
        lines_ ,= Line_('b', " = ", Afrund_(model['b'], _rest) );
    end if;
    if HasOption_(['konfidensinterval_b', 'konfint_b'], _rest) then
        lines_ ,= Line_("konfidensinterval for b : ", Interval_(model['konfint_b'], _rest) );
    end if;
    if HasOption_(['standardafvigelse_b', 'stdafv_b'], _rest) then
        lines_ ,= Line_("standardafvigelse for b : ", Afrund_(model['stdafv_b'], _rest) );
    end if;
    if HasOption_('c', _rest) then
        lines_ ,= Line_('c', " = ", Afrund_(model['c'], _rest) );
    end if;
    if HasOption_(['konfidensinterval_c', 'konfint_c'], _rest) then
        lines_ ,= Line_("konfidensinterval for c : ", Interval_(model['konfint_c'], _rest) );
    end if;
    if HasOption_(['standardafvigelse_c', 'stdafv_c'], _rest) then
        lines_ ,= Line_("standardafvigelse for c : ", Afrund_(model['stdafv_c'], _rest) );
    end if;
    if HasOption_('k', _rest) then
        lines_ ,= Line_('k', " = ", Afrund_(model['k'], _rest) );
    end if;
    if HasOption_(['konfidensinterval_k', 'konfint_k'], _rest) then
        lines_ ,= Line_("konfidensinterval for k : ", Interval_(model['konfint_k'], _rest) );
    end if;
    if HasOption_(['standardafvigelse_k', 'stdafv_k'], _rest) then
        lines_ ,= Line_("standardafvigelse for k : ", Afrund_(model['stdafv_k'], _rest) );
    end if;
    if HasOption_(['koefficienter', 'koef'], _rest) then
        arr_ := model['koef'];
        arr_ := map(x -> ('a'[op(x)] = arr_[op(x)], ", "), [indices(arr_)]);
        lines_ ,= Line_(op(arr_));
    end if;
    if HasOption_(['konfidensintervaller', 'konfint'], _rest) then
        arr_ := model['konfint'];
        arr_ := map(x -> ('a'[op(x)], ": ", Interval_(eval(arr_[op(x)])), ", "), [indices(arr_)]);
        lines_ ,= Line_("Konfidensintervaller: ", op(arr_));
    end if;
    if HasOption_(['standardafvigelser', 'stdafv'], _rest) then
        arr_ := model['stdafv'];
        arr_ := map(x -> ('a'[op(x)], ": ", arr_[op(x)], ", "), [indices(arr_)]);
        lines_ ,= Line_("Standardafvigelser: ", op(arr_));
    end if;
    Print_(lines_);
end proc;

###################
# 
# Interval_(interval [, opts])
#
# Bruges af PrintFunk_ til at skrive a..b som [a, b]
#
# - interval er en Maple-range (a..b)
# - opts videregives til Afrund_
#
Interval_ := proc(interval::`..`)
    local min_, max_;
    min_, max_ := op(Afrund_(interval, _rest));
    [min_, max_];
end proc;


###################
# 
# Model_(m, vars [,opts])
# 
# Bruges af regressionsfunktionerne til at formattere output.
#
# - m er et modul, som returneres af en af Maples Fit-funktioner
# - vars er en liste med navnene på den uafhængige og den afhængige variabel
#
# Valgfri ekstra argumenter:
# - 'cifre'=n angiver antal betydende cifre i det udskrevne resultat 
#     og i tabellen d, men påvirker ikke den returnerede funktion.
# - 'parametre'=n angiver antallet af fittede parametre
# - 'eksponentiel' angiver, at resultatet vises på formen b*a^x
# - 'transform' angiver, at data er blevet transformeret før fitning, og 
#     at de fundne parametre skal transformeres tilbage 
#     (eksponentiel og potens regression)
# - 'poly'=n angiver, at den fittede funktion er et n'te grads polynomium.
# - 'vis' angiver, at den fittede funktion skal printes
# - 'detaljer' angiver, at tabellen d også skal returneres
# - 'rkvadrat' angiver, at r-i-anden også skal udskrives
#
# Output:
#   den fittede funktion
#   evt. tabellen d, som indeholder div. parametre (afrundet) - 
#     kun hvis 'detaljer' angives:
#   'a': alle regressioner undtagen den generelle polynomiumsregression
#   'stdafv_a' = 'standardafvigelse_a', 'konfint_a' = 'konfidensinterval_a'
#   'b': konstantled i lineær, eksponentiel, potensregression; 
#        koef. til x i andengradsreg.
#   'stdafv_b' = 'standardafvigelse_b', 'konfint_b' = 'konfidensinterval_b'
#   'c': konstantled i andengradsregression
#   'stdafv_c' = 'standardafvigelse_c', 'konfint_c' = 'konfidensinterval_c'
#   'k': parameter i e^kx
#   'stdafv_k' = 'standardafvigelse_k', 'konfint_k' = 'konfidensinterval_k'
#   'koef' = 'koefficienter': funktion, som giver giver kofficienterne i et 
#       fittet polynomium med sædvanlig nummerering (a_0, a_1, ..., a_n)
#   'stdafv' = 'standardafvigelser', 'konfint' = 'konfidensintervaller'
#   'r2' = 'rkvadrat': r-i-anden for den fittede funktion.
#
Model_ := proc(m, vars)
    local _d, _udtryk, _coefs, _n, _opts, juster_;
    _opts := _rest, '_def' = Digits;
    _n := FindOption_('parametre', _rest, '_def' = 0);
    _d := table();
    
    if _n > 0 then
        if HasOption_('naturlig', _rest) then
            _d['k'] := Afrund_(m:-Results('parametervector')[-1], _opts);
            _d['stdafv_k'] := Afrund_(
                m:-Results('standarderrors')[-1], 
                _opts
            );
            _d['konfint_k'] := Afrund_(
                m:-Results('confidenceintervals')[-1], 
                _opts
            );
            _d['standardafvigelse_k'] := _d['stdafv_k'];
            _d['konfidensinterval_k'] := _d['konfint_k'];
        else
            if HasOption_('eksponentiel', _rest) then
                _d['a'] := Afrund_(
                    exp(m:-Results('parametervector')[-1]), 
                    _opts
                );
                _d['stdafv_a'] := Afrund_(
                    exp(m:-Results('standarderrors')[-1]), 
                    _opts
                );
                _d['konfint_a'] := Afrund_(
                        map(exp, m:-Results('confidenceintervals')[-1]), 
                        _opts
                );
            else
                _d['a'] := Afrund_(
                    m:-Results('parametervector')[-1], 
                    _opts
                );
                _d['stdafv_a'] := Afrund_(
                    m:-Results('standarderrors')[-1], 
                    _opts
                );
                _d['konfint_a'] := Afrund_(
                    m:-Results('confidenceintervals')[-1], 
                    _opts
                );
            end if;
            _d['standardafvigelse_a'] := _d['stdafv_a'];
            _d['konfidensinterval_a'] := _d['konfint_a'];
        end if;
        if _n > 1 then
            if HasOption_('transform', _rest) then
                _d['b'] := Afrund_(
                    exp(m:-Results('parametervector')[-2]), 
                    _opts
                );
                _d['stdafv_b'] := Afrund_(
                    exp(m:-Results('standarderrors')[-2]), 
                    _opts
                );
                _d['konfint_b'] := Afrund_(
                    map(exp, m:-Results('confidenceintervals')[-2]), 
                    _opts
                );
            else
                _d['b'] := Afrund_(
                    m:-Results('parametervector')[-2], 
                    _opts
                );
                _d['stdafv_b'] := Afrund_(
                    m:-Results('standarderrors')[-2], 
                    _opts
                );
                _d['konfint_b'] := Afrund_(
                    m:-Results('confidenceintervals')[-2], 
                    _opts
                );
            end if;
            _d['standardafvigelse_b'] := _d['stdafv_b'];
            _d['konfidensinterval_b'] := _d['konfint_b'];
            if _n > 2 then
                _d['c'] := Afrund_(
                    m:-Results('parametervector')[-3], 
                    _opts
                );
                _d['stdafv_c'] := Afrund_(
                    m:-Results('standarderrors')[-3], 
                    _opts
                );
                _d['konfint_c'] := Afrund_(
                    m:-Results('confidenceintervals')[-3], 
                    _opts
                );
                _d['standardafvigelse_c'] := _d['stdafv_c'];
                _d['konfidensinterval_c'] := _d['konfint_c'];
            end if;
        end if;
    else
        juster_ := proc(arr) 
        # afrund og start indeks i 0
            local tmp_, i_;
            for i_ to nops(arr) do
                tmp_[i_-1] := Afrund_(arr[i_], _rest);
            end do;
            tmp_;
        end proc;
        
        _n := FindOption_('poly', _rest, _def=-1);
        _d['koef'] := juster_(m:-Results('parametervector'), _rest);
        _d['koefficienter'] := _d['koef'];
        _d['stdafv'] := juster_(m:-Results('standarderrors'), _rest);
        _d['standardafvigelser'] := _d['stdafv'];
        _d['konfint'] := juster_(m:-Results('confidenceintervals'), _rest);
        _d['konfidensintervaller'] := _d['konfint'];
    end if;
    _d['rkvadrat'] := Afrund_(m:-Results('rsquared'), _opts);
    _d['r2'] := _d['rkvadrat'];
    if HasOption_('eksponentiel', _rest) then
        _coefs := m:-Results('parametervector');
        _udtryk := unapply(
            exp(_coefs[1])*(exp(_coefs[2]))^(vars[1]), 
            vars[1]
        );
    else
        _udtryk := unapply(m:-Results('leastsquaresfunction'), vars[1]);
    end if;
    PrintFunk_(_udtryk(vars[1]), op(vars), _d, _opts);
    if HasOption_('detaljer', _rest) then
        eval(_udtryk), _d;
    else
        eval(_udtryk);
    end if;
end proc;



###################
# 
# TilpasMatrix_(M)
# 
# Sørger for, at M er en 2 x n-matrix 
#
TilpasMatrix_ := proc( M::Matrix )
    if op(1, M)[1] > 2 and op(1, M)[2] = 2 then
        M^+;
    else
        M;
    end;
end proc;


###################
# 
# LineærModel(M)
# 
# Fitter data i 2 x N-matrix til en funktion af formen ax+b 
# se Model_ for detaljer
#
LineærModel := proc( M::Matrix )
    local _mat, _m, _vars;
    _vars := FindOption_('variable', _rest, '_def' = ['x','f']);
    _mat := TilpasMatrix_(M);
    
    _m := Statistics:-PolynomialFit(
        1, _mat[1], _mat[2], _vars[1], 'output' = 'solutionmodule'
    );
    Model_(_m, _vars, 'parametre' = 2, _rest);
end proc:


###################
# 
# EksponentielModel(M)
# 
# Fitter data i 2 x N-matrix til b*e^(kx) (eller b*a^x)
# se Model_ for detaljer
#
EksponentielModel := proc( M::Matrix )
    local _mat, _m, _vars;
    _vars := FindOption_('variable', _rest, '_def' = ['x','f']);
    _mat := TilpasMatrix_(M);
    
    _m := Statistics:-ExponentialFit(
        _mat[1], _mat[2], _vars[1], 'output' = 'solutionmodule'
    );
    if HasOption_('naturlig', _rest) then
        Model_(_m, _vars, 'transform', 'parametre' = 2, _rest);
    else
        Model_(_m, _vars, 'transform', 'eksponentiel', 'parametre'=2, _rest);
    end if;
end proc;


###################
# 
# PotensModel(M)
# 
# Fitter data i 2 x N-matrix til en funktion af formen b*x^a
# se Model_ for detaljer
#
PotensModel := proc( M::Matrix )
  local _mat, _m, _vars;
  _vars := FindOption_('variable', _rest, '_def' = ['x','f']);
   _mat := TilpasMatrix_(M);
 
  _m := Statistics:-PowerFit(
    _mat[1], _mat[2], _vars[1], 'output' = 'solutionmodule'
  );
  Model_(_m, _vars, 'transform', 'potens', 'parametre' = 2, _rest);
end proc;


###################
# 
# ProportionelModel(M)
# 
# Fitter data i 2 x N-matrix til en funktion af formen ax
# se Model_ for detaljer
#
ProportionelModel := proc( M::Matrix )
  local _mat, _m, _vars;
  _vars := FindOption_('variable', _rest, '_def' = ['x','f']);
  _mat := TilpasMatrix_(M);

  _m := Statistics:-LinearFit(
      [_vars[1]], M[1], M[2], _vars[1], 'output' = 'solutionmodule'
  );
  Model_(_m, _vars, 'parametre' = 1, _rest);
end proc;


###################
# 
# KvadratiskModel(M)
# 
# Fitter data i 2 x N-matrix til en funktion af formen ax^2
# se Model_ for detaljer
#
KvadratiskModel := proc( M::Matrix )
  local _mat, _m, _vars;
  _vars := FindOption_('variable', _rest, '_def' = ['x','f']);
  _mat := TilpasMatrix_(M);

  _m := Statistics:-LinearFit(
      [_vars[1]^2], _mat[1], _mat[2], _vars[1], 'output' = 'solutionmodule'
  );
  Model_(_m, _vars, 'parametre' = 1, _rest);
end proc;


###################
# 
# ReciprokModel(M)
# 
# Fitter data i 2 x N-matrix til en funktion af formen a/x
# se Model_ for detaljer
#
ReciprokModel := proc( M::Matrix )
  local _mat, _m, _vars;
  _vars := FindOption_('variable', _rest, '_def' = ['x','f']);
  _mat := TilpasMatrix_(M);

  _m := Statistics:-LinearFit(
      [1/_vars[1]], _mat[1], _mat[2], _vars[1], 'output' = 'solutionmodule'
  );
  Model_(_m, _vars, 'parametre' = 1, _rest);
end proc;


###################
# 
# AndengradsModel(M)
# 
# Fitter data i 2 x N-matrix til en funktion af formen ax^2+bx+c
# se Model_ for detaljer
#
AndengradsModel := proc( M::Matrix )
  local _mat, _m, _vars;
  _vars := FindOption_('variable', _rest, '_def' = ['x','f']);
  _mat := TilpasMatrix_(M);

  _m := Statistics:-PolynomialFit(
      2, _mat[1], _mat[2], _vars[1], 'output' = 'solutionmodule'
  );
  Model_(_m, _vars, 'parametre' = 3, _rest);
end proc;


###################
# 
# PolynomiumsModel(M, n)
# 
# Fitter data i 2 x N-matrix til en funktion af formen 
#     a_0 + a_1*x + a_2*x^2 + ... + a_n*x^n
# se Model_ for detaljer
#
PolynomiumsModel := proc( M::Matrix, deg::nonnegint )
  local _mat, _m, _vars;
  _vars := FindOption_('variable', _rest, '_def' = ['x','f']);
  _mat := TilpasMatrix_(M);

  _m := Statistics:-PolynomialFit(
      deg, _mat[1], _mat[2], _vars[1], 'output' = 'solutionmodule'
  );
  Model_(_m, _vars, 'poly' = deg, _rest);
end proc;


###################
# 
# Residual(M, f, opts)
# 
# Laver et residualplot og beregner residualspredningen
# - M er en 2 x N-matrix eller N x 2-matrix af datapunkter
# - f er en modelfunktion
# - opts er et eller flere af tilvalgene
#   x= <interval>
#   cifre= <antal bet. cifre)
#
# Returnerer en liste med alle residualerne
#
Residual := proc( M::Matrix, f::procedure )
    local _mat, _n, _diff, _pts, _x_int, _plot, _spr;
    
    _mat := TilpasMatrix_(M);
    _n := op(1, _mat)[2];
    _diff := zip((x, y) -> y - f(x), _mat[1], _mat[2]);
    _pts := zip((x, y) -> [x, y], _mat[1], _diff);
    _x_int := FindOption_('x', _rest, '_def'=XInterval_([_mat], FAIL, []));
    _plot := plot('x'=_x_int),      # dummy plot for at angive x-aksen
            plots:-pointplot(_pts);
    _spr := sqrt(add(map(x -> x^2, _diff)) / (_n - 2));
    _plot := _plot, plot(_spr, 'x'=_x_int, 'linestyle'='dot'), 
                    plot(-_spr, 'x'=_x_int, 'linestyle'='dot'),
                    plot(2*_spr, 'x'=_x_int, 'linestyle'='dot'),
                    plot(-2*_spr, 'x'=_x_int, 'linestyle'='dot');
    Plot_(plots:-display(_plot), Line_("residualspredning: ", Afrund_(_spr, _rest)));
    _diff;
end proc;


###################
# 
# Rkvadrat(M, f)
# 
# Beregner R^2 for funktione f givet datapunkterne i M
#
Rkvadrat := proc(M::Matrix,f::procedure)
  local _mat, _err, _n;
  _mat := TilpasMatrix_(M);
  _n := op(1, _mat)[2];

  _err := zip( (x,y)->y-f(x), _mat[1], _mat[2]);
  1 - _err._err/(_mat[2]._mat[2] - _n * Statistics:-Mean(_mat[2])^2)
end proc:

###################
# 
# IndlæsExcelData(fil, område [, sti])
#
# Indlæser data fra et Excel-regneark
# - fil er filnavnet
# - område er en tekststreng af formen "Xn:Ym", som angiver hvor data ligger
# - sti er stien til downloadmappen
#
IndlæsExcelData := proc(fil::string, område::string := "")
    local home_, fil_, sti_, data_;

    use FileTools in
        if HasOption_('sti', _rest) then
            sti_ := FindOption_('sti', _rest);
        else
            home_ := JoinPath([kernelopts(homedir), "Downloads"]);
            if not Exists(home_) then
                home_ := JoinPath([kernelopts(homedir), "Overførsler"]); # på nogle Mac's
                if not Exists(home_) then
                    home_ := JoinPath([kernelopts(homedir), "Hentet"]); # på nogle Linuxer
                    if not Exists(home_) then
                        Err_("Dit system bruger ikke den almindelige download-mappe. ",
                            "Brug tilvalget 'sti=...' til at vise, hvor downloadede filer gemmes"
                        );
                    end if;
                end if;
            end if;
        end if;
        fil_ := fil;
        if not Exists(JoinPath([home_, fil_])) then
            fil_ := cat(fil_, ".xlsx");
        end;
        fil_ := JoinPath([home_, fil_]);
        if not Exists(fil_) then
            Err_("Kan ikke finde excelfilen ", 
                fil_,
                ". Husk at den skal ligge i download-mappen"
            );
        end if;
    end use;
    data_ := ExcelTools:-Import(fil_, 1, område);
    if op(1, data_)[1] = 1 or op(1, data_)[2] = 1 then    # 1-dim data
        convert(data_, list);
    else
        data_;
    end;
end proc;
