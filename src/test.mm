#############################################################################
#                                                                           #
# Copyright (C) 2018 by Claus Christensen                                   #
# This file is part of KGModul                                              #
#                                                                           #
# This program is free software: you can redistribute it and/or modify      #
# it under the terms of the GNU General Public License as published by      #
# the Free Software Foundation, either version 3 of the License, or         #
# (at your option) any later version.                                       #
#                                                                           #
# This program is distributed in the hope that it will be useful,           #
# but WITHOUT ANY WARRANTY; without even the implied warranty of            #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
# GNU General Public License for more details.                              #
#                                                                           #
# You should have received a copy of the GNU General Public License         #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    #
#                                                                           #
#############################################################################

#############################################################################
#
# Denne fil er ikke en del af KGModul pakken. Den rummer procedurer, som
# bruges i test-dokumentet.
#
#############################################################################


#################################
#
# _getIniFile
#
# Returner den fulde sti til ini-filen som en streng
#
_getIniFile := proc()
    local _iniName;
    
    if kernelopts(platform) = "unix" then  # Linux og Mac
        _iniName := ".mapleinit";
    else
        _iniName := "maple.ini";    # Windows
    end if;
    FileTools:-JoinPath([kernelopts(homedir),_iniName]);
end proc;


#################################
#
# _WindowsPatch(sti)
#
# Omformer en windows-sti til noget som giver mening som streng
# (erstatter '\' med '/'.
#
_WindowsPatch := proc(sti)
    if kernelopts(platform) = "unix" then
        sti;
    else
        StringTools:-Join(FileTools:-SplitPath(sti), "/");
    end if;
end proc;


#################################
#
# SetupTest
#
# Skriver linjer i enden af ini-filen
#
SetupTest := proc()
    local _iniFile, _fh;
    _iniFile := _getIniFile();
    
    use FileTools:-Text in
        _fh := Open(_iniFile, 'append');
        WriteLine(_fh,  # efter restart skal vi
                # .. huske testSrc
            cat("testSrc:=", testSrc, ":"),
                # .. huske sti til src
            cat("KGModulKildeSti:=\"", _WindowsPatch(KGModulKildeSti), "\":"),
            "currentdir(KGModulKildeSti):",
                # .. indlæse src/test.mm
            "read \"src/test.mm\":",
                # .. indlæse src/KGModul.mpl, hvis vi tester kildkode
            "if testSrc = true then",
            "    read \"src/KGModul.mpl\":",
            "else",
            "    read \"src/KGModul-test.mpl\":",
            "end if:",
                # .. sikre korrekt afrunding
            "Rounding:='simple':",
                # .. indlæse KGModul
            "with(KGModul):"
        );
        Close(_fh);
    end use;
end proc;


#################################
#
# TeardownTest
#
#################################

TeardownTest := proc()
    local _iniFile, _fh, _contents, _lines, split;
    _iniFile := _getIniFile();
    split := StringTools:-Split;
    
    use FileTools:-Text in
        _fh := Open(_iniFile);
        _contents := ReadFile(_fh);
        Close(_fh);
        _lines := split(_contents, "\n");
        _fh := Open(_iniFile, 'overwrite');
        WriteLine(_fh, op(_lines[..-13]));
        Close(_fh);
    end use;
end proc;


####################################
#
# Test(expr, comp)
#
#  Som evalb(expr=comp), men med tydelig fejlvisning og håndtering af vektorer
#
####################################

Test := proc(expr, comp)
    option inline;
    if not type(expr, 'Vector') and evalb(expr = comp) then
        true;
    elif evalb(convert(expr, 'list') = convert(comp, 'list')) then
        true;
    else
        error("\t\t%1 <> %2", expr, comp);
    end if;
end proc;
