#############################################################################
#                                                                           #
# Copyright (C) 2018 by Claus Christensen                                   #
# This file is part of KGModul                                              #
#                                                                           #
# This program is free software: you can redistribute it and/or modify      #
# it under the terms of the GNU General Public License as published by      #
# the Free Software Foundation, either vers_ion 3 of the License, or         #
# (at your option) any later vers_ion.                                       #
#                                                                           #
# This program is distributed in the hope that it will be useful,           #
# but WITHOUT ANY WARRANTY; without even the implied warranty of            #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
# GNU General Public License for more details.                              #
#                                                                           #
# You should have received a copy of the GNU General Public License         #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    #
#                                                                           #
#############################################################################

###################
# Info()
#
# Udskriver information om programmet
#
Info := proc()
    Print_(TextLines_(
        cat(PakkeNavn_, ": Køge Gymnasiums hjælpepakke til Maple"), 
        "",
        cat(PakkeNavn_, " version ", Version_, ", ", Date_, ", ", Copyright_),
        cat(PakkeNavn_, " er fri software ",
            "udgivet under GNU General Public License version 3"),
        "",
        cat("Skriv  ?", PakkeNavn_, ",Oversigt  for at få en oversigt over pakkens funktioner")
    ));
end proc;


###################
# Init()
#
# Skriver div. nyttige kommandoer til brugerens maple.ini / .mapleinit
#
Init := proc()
    local filNavn_, iniNavn_, fh_, indhold_, startStreng_, slutStreng_, skip_, prefix_, vers_, line_;
    
    if kernelopts(platform) = "unix" then
        iniNavn_ := ".mapleinit";
    else
        iniNavn_ := "maple.ini";
    end if;
    skip_ := false;
    startStreng_ := "# KGModul: ini-fil v ";
    slutStreng_ := "# end KGModul";
    vers_ := "";
    
    use FileTools, FileTools:-Text, StringTools in
        filNavn_ := JoinPath([kernelopts(homedir),iniNavn_]);
        if Exists(filNavn_) and Size(filNavn_) > 0 then
            fh_ := Open(filNavn_);
            indhold_ := ReadFile(fh_);
            Close(fh_);
        else
            indhold_ := "";
        end if;
        fh_ := Open(filNavn_, overwrite=true);
        
        # write all foreign lines back but skip_ all KGModul lines
        for line_ in Split(indhold_, "\r\n") do
            if not skip_ then
                prefix_ := SubString(line_, 1..Length(startStreng_));
                if prefix_ = startStreng_ then
                    skip_ := true;
                    vers_ := SubString(line_, (Length(startStreng_)+1)..(Length(startStreng_)+3));
                else
                    WriteLine(fh_, line_);
                end if;
            else
                prefix_ := SubString(line_, 1..Length(slutStreng_));
                if prefix_ = slutStreng_ then
                    skip_ := false;
                end if;
            end if;
        end do;
        if vers_ = "" then 
            vers_ := "00";
        end if;
        
        # write KGModul lines
        WriteLine(fh_, sprintf("\n%s%02d", startStreng_, parse(vers_)+1));
        WriteLine(fh_, "local O:");
        WriteLine(fh_, "interface(imaginaryunit=`_I`):");
        WriteLine(fh_, "interface(typesetting=extended):");
        WriteLine(fh_, "interface(rtablesize=15):");
        WriteLine(fh_, "Rounding:='simple':");
        WriteLine(fh_, "ScientificConstants:-ModifyConstant(g, value=9.82,uncertainty=0.01):");
        WriteLine(fh_, "Units:-UseContexts(SI,metric):");
        WriteLine(fh_, slutStreng_);
        
        Close(fh_);
    end use;
end proc;
