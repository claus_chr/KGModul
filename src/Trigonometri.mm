#############################################################################
#                                                                           #
# Copyright (C) 2018 by Claus Christensen                                   #
# This file is part of KGModul                                              #
#                                                                           #
# This program is free software: you can redistribute it and/or modify      #
# it under the terms of the GNU General Public License as published by      #
# the Free Software Foundation, either version 3 of the License, or         #
# (at your option) any later version.                                       #
#                                                                           #
# This program is distributed in the hope that it will be useful,           #
# but WITHOUT ANY WARRANTY; without even the implied warranty of            #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
# GNU General Public License for more details.                              #
#                                                                           #
# You should have received a copy of the GNU General Public License         #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    #
#                                                                           #
#############################################################################

###################
# tilGrad(v)
#
# Omregner vinkel i radianer til grader (men u. enhed)
#
tilGrad := proc(u)
    local v;
    v := convert(u, 'degrees')/'degrees';
    if isFloat_(u) then
        v := Afrund_(v, 'cifre' = Digits + 4);
    end if;
    v;
end proc:


###################
# tilRad(v)
#
# Omregner vinkel i grader (u. enhed) til radianer
#
tilRad := proc(v)
    local u;
    u := convert(v * 'degrees','radians');
    if isFloat_(v) then
        u := Afrund_(u, 'cifre' = Digits + 4);
    end if;
    u;
end proc:


###################
# Cos(v)
#
# Finder cos til en vinkel angivet i grader (u. enhed)
#
Cos := proc(x)
    Digits := Digits + 4;
    cos(tilRad(x))
end proc;


###################
# Sin(v)
#
# Finder sin til en vinkel angivet i grader (u. enhed)
#
Sin := proc(x)
    Digits := Digits + 4;
    sin(tilRad(x))
end proc;


###################
# Tan(v)
#
# Finder tan til en vinkel angivet i grader (u. enhed)
#
Tan := proc(x)
    Digits := Digits + 4;
    tan(tilRad(x))
end proc;


###################
# InvCos(x)
#
# Finder arccos i grader (u. enhed) til et tal
#
InvCos := proc(x)
    Digits := Digits + 4;
    if evalf(x) < -1 or evalf(x) > 1 then
        error "Argumentet skal ligge imellem -1 og 1; fik %1", x
    end if;
    tilGrad(arccos(x));
end proc;


###################
# InvSin(x)
#
# Finder arcsin i grader (u. enhed) til et tal
#
InvSin := proc(x)
    Digits := Digits + 4;
    if evalf(x) < -1 or evalf(x) > 1 then
        error "Argumentet skal ligge imellem -1 og 1; fik %1", x
    end if;
    tilGrad(arcsin(x));
end proc;


###################
# InvTan(x)
#
# Finder arctan i grader (u. enhed) til et tal
#
InvTan := proc(x)
    Digits := Digits + 4;
    tilGrad(arctan(x));
end proc;
