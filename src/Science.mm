#############################################################################
#                                                                           #
# Copyright (C) 2018 by Claus Christensen                                   #
# This file is part of KGModul                                              #
#                                                                           #
# This program is free software: you can redistribute it and/or modify      #
# it under the terms of the GNU General Public License as published by      #
# the Free Software Foundation, either version 3 of the License, or         #
# (at your option) any later version.                                       #
#                                                                           #
# This program is distributed in the hope that it will be useful,           #
# but WITHOUT ANY WARRANTY; without even the implied warranty of            #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
# GNU General Public License for more details.                              #
#                                                                           #
# You should have received a copy of the GNU General Public License         #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    #
#                                                                           #
#############################################################################

use ScientificConstants in
    ###################
    # 
    # Konstant(symb)
    # 
    # Returnerer værdien for naturkonstanten symb
    #
    # - symb er Maples symbol for en naturkonstant
    #
    Konstant := proc(symb)
        local _k, _value, _err, _rund, _enh;

        _k := Constant(symb);
        _value := GetValue(_k);
        _err := GetError(_k);
        _enh := GetUnit(_k);
        if _err = 0 then 
            return _value * _enh;
        end if;
        _rund := abs(floor(log[10](_err / _value)));
        Afrund_(_value, 'cifre' = _rund) * _enh;
    end proc;
    
    ###################
    # 
    # IsotopMasse(navn, A)
    # 
    # Finder isotopmasser
    #
    # - navn er grundstoffets kemiske symbol eller navn
    # - A er nukleontallet
    #
    Isotopmasse := proc( navn, A)
        local _isotop;
        _isotop := Element( navn[A], 'atomicmass', 'system' = 'SI');
        GetValue(_isotop) * GetUnit(_isotop);
    end proc;
    
    ###################
    # 
    # _MolarMasse(stof)
    # 
    # Finder den molare masse af et grundstof
    #
    # stof er et grundstofsymbol
    #
    Molarmasse_ := proc( symb )
        local _element, _avogadro;
        _element := Element(symb, 'atomicweight', 'system' = 'SI');
        _element := GetValue(_element) * GetUnit(_element);
        _avogadro := Constant('N[A]', 'units', 'system' = 'SI');
        convert(simplify(evalf(_element * _avogadro)), 'units', 'g/mol');
    end proc:

    ###################
    # 
    # Molarmasse(stof)
    # 
    # Finder den molare masse af et stof
    #
    # stof er et grundstofsymbol eller en kemisk formel
    #
    Molarmasse := proc( stof )
        local _m, _elt;
        _m := 0;
        if type(stof, 'symbol') then
            _m := Molarmasse_(stof);
        elif type(stof, 'indexed') then
            _m := Molarmasse_(op(0,stof)) * op(stof);
        else
            for _elt in stof do
                # Hvis samme grundstofsymbol optræder flere gange i formlen, så samler 
                # Maple dem som et potensudtryk
                if op(0, _elt) = `^` then
                    _m := _m + op(2, _elt) * Molarmasse(op(1,_elt));
                else
                    _m := _m + Molarmasse(_elt);
                end if;
            end do;
        end if;
        _m;
    end proc;

end use;

###################
# 
# GemData(data, enhed)
# 
# Gem tabel med data samt deres enhed som <data>.mpd
#
# Se eksempel på anvendelse i Varmelære.mw
#
GemData := proc(data::evaln, enhed)
    local _save_data, _save_enhed, _save_dekade;
    
    _save_data := eval(data);
    _save_enhed := eval(enhed);
    _save_dekade := FindOption_('dekade', _rest, '_def' = 0);
    save _save_data, _save_enhed, _save_dekade, SaveName_(data);
end proc;

###################
# 
# HentData(_Data)
# 
# Hent datatabel gemt med GemData
#
# Se eksempel på anvendelse i Varmelære.mw
#
HentData := proc(_Data::evaln)
    local _fil, _data, _enhed, _dekade, _i;
    
    _fil := SaveName_(_Data);
    read _fil;
    _data := eval(_save_data);
    _enhed := eval(_save_enhed);
    _dekade := eval(_save_dekade);
    for _i in indices(_data) do
        _data[op(_i)] := convert(_data[op(_i)] * 1.*10^_dekade * Unit(_enhed), 'units', _enhed);
    end do;
    eval(_data);
end proc;

