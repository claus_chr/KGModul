#############################################################################
#                                                                           #
# Copyright (C) 2018 by Claus Christensen                                   #
# This file is part of KGModul                                              #
#                                                                           #
# This program is free software: you can redistribute it and/or modify      #
# it under the terms of the GNU General Public License as published by      #
# the Free Software Foundation, either version 3 of the License, or         #
# (at your option) any later version.                                       #
#                                                                           #
# This program is distributed in the hope that it will be useful,           #
# but WITHOUT ANY WARRANTY; without even the implied warranty of            #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
# GNU General Public License for more details.                              #
#                                                                           #
# You should have received a copy of the GNU General Public License         #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    #
#                                                                           #
#############################################################################

use DocumentTools, DocumentTools:-Layout in

###################
# 
# Line_(sekv)
# 
# Danner et Textfield baseret på en sekvens af strenge og udtryk,
# som kan udskrives med Print_. Hele listen bliver til en linje
#
# - sekv er en sekvens af strenge og udtryk
#
Line_ := proc()
    local _fmt;
    _fmt := proc(x)
        if type(x, 'string') then
            x;
        else
            Equation(x);
        end if;
    end proc;
    Textfield(op(map(_fmt, [_rest])));
end proc;

###################
# 
# TextLines_(sekv)
# 
# Danner en sekvens af Textfield's ud fra en sekvens af strenge,
# som kan udskrives med Print_. Hver streng bliver en ny linje
#
# - sekv er en sekvens af tekststrenge
#
TextLines_ := proc()
    op(map(Textfield, [_rest]));
end proc;

###################
#
# Print_(txts)
#
# Printer en sekvens af Textfield's på hver sin linje
#
# - txts er en sekvens af Textfield's
#
Print_ := proc()
    InsertContent(Worksheet(DocumentBlock(Group(_rest))));
end proc;


###################
#
# Plot_(p, txts)
#
# Printer et plot fulgt af en sekvens af Textfield's på hver sin linje
#
# - p er en plot-struktur
# - txts er en sekvens af Textfield's
#
Plot_ := proc(p)
    InsertContent(Worksheet(Table(Column(), 
        Row(InlinePlot(p)), 
        op(map(Row, [_rest])),
        'interior'='none', 'exterior'='none'
    )));
    NULL;  # Undgå at tabel-strukturen returneres
end proc;
    
end use; # DocumentTools, Layout
