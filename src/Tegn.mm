#############################################################################
#                                                                           #
# Copyright (C) 2018 by Claus Christensen                                   #
# This file is part of KGModul                                              #
#                                                                           #
# This program is free software: you can redistribute it and/or modify      #
# it under the terms of the GNU General Public License as published by      #
# the Free Software Foundation, either version 3 of the License, or         #
# (at your option) any later version.                                       #
#                                                                           #
# This program is distributed in the hope that it will be useful,           #
# but WITHOUT ANY WARRANTY; without even the implied warranty of            #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
# GNU General Public License for more details.                              #
#                                                                           #
# You should have received a copy of the GNU General Public License         #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    #
#                                                                           #
#############################################################################

            
###################
# 
# Farve(nr) 
#
# Vælger en farve i den interne liste med farver
#
# nr er et (vilkårligt) helt tal
# returnerer en farvevalgsoption, som kan bruges med Maples plot-kommandoer
#
farve_ := 0;
Farve := proc()
    local farver_;

    farver_ := ["Black", "Blue", "Violet", "Lime", "Green", "Cyan", "Red", 
            "Magenta", "Gold", "Brown"];

    if _npassed = 1 then
        if type(_passed[1], 'posint') then
            farve_ += (-farve_) + _passed[1] - 1;   # kan ikke tildele men godt opdatere farve_ !?
        else
            farve_ -= farve_;   # Nulstil farve, så vi er klar til næste kald
            return;
        end if;
    end if;

    farve_ += 1;
    'color' = farver_[(farve_-1) mod 10 + 1];
end proc;


###################
# 
# XInterval_(l, pts, plots) 
# 
# Bruges af Graf til at finde intervallet til førsteaksen
#
# l er funktioner og datamatricer
# pts er et punkt eller en liste med punket
# plots er en liste med plotstrukturer
#
# returnerer x-interval bestemt fra datamatricer, funktionsplot og punkter
# hvis der er nogen; ellers std. interval. Intervallet inkluderer altid 0
#
XInterval_ := proc(l, pts, plots)
    local _xmin, _xmax, _M, _d, _interval, _pts, _pt;
    _xmin := infinity;
    _xmax := -infinity;
    
    # Undersøg datasæt for største og mindste x-værdi
    for _M in l do
        # Kun datasæt skal undersøges for start- og slutpunkt
        if type(_M, 'Matrix') then
            _M := evalf(_M);        # Udregn 'symbolske' udtryk som fx cos(1)
            # Find mindste og største x-værdi blandt punkterne i matricen
            if min(_M[1]) < _xmin then _xmin := min(_M[1]) end if;
            if max(_M[1]) > _xmax then _xmax := max(_M[1]) end if;
        end if;
    end do;

    # Undersøg parameterplot for største og mindste x-værdi
    for _M in plots do
        local _data := (op(1, op(1, _M))^+)[1]; # x-koordinaterne i plottet
        if min(_data) < _xmin then _xmin := min(_data); end if;
        if max(_data) > _xmax then _xmax := max(_data); end if;
    end do;

    # Undersøg enkeltpunkter for største og mindste x-værdi
    if not pts = FAIL then
        _pts := evalf(pts);         # Udregn 'symbolske' udtryk
        # Hvis aksen ikke er tilpasset: Brug std.interval medmindre et punkt falder udenfor
        if _xmin = infinity then 
            _xmin := -10; 
        end if;
        if _xmax = -infinity then
            _xmax := 10;
        end if;
        # Inddrag også enkeltpunkter
        if type(_pts[1], list) then       # liste med punkter
            for _pt in _pts do
                if _pt[1] < _xmin then _xmin := _pt[1] end if;
                if _pt[1] > _xmax then _xmax := _pt[1] end if;
            end do;
        else                                   # enkelt punkt
            if _pts[1] < _xmin then _xmin := _pts[1] end if;
            if _pts[1] > _xmax then _xmax := _pts[1] end if;
        end if;
    end if;

    if _xmax > -infinity then
        # Datapunkter bestemmer x-akse
        if _xmin > 0 then _xmin := 0; end if;
        if _xmax < 0 then _xmax := 0; end if;
        _d := _xmax - _xmin;
        _xmin := _xmin - 0.1*_d;
        _xmax := _xmax + 0.1*_d;
        _interval := _xmin.._xmax;
    else                                # Brug standard x-akse
        _interval := -10..10;
    end if;
    _interval;
end proc:


###################
# 
# YInterval_(l, pts) 
# 
# Bruges af Graf til at finde intervallet til andenaksen
#
# l er en liste med Maple-plotstrukturer
# pts er et punkt eller en liste med punket
#
# returnerer y-interval bestemt fra datamatricer, funktionsplot og punkter
# hvis der er nogen; ellers std. interval. Intervallet inkluderer altid 0
#
YInterval_ := proc(l, pts)
    local _ymin, _ymax, _p, _data, _d, _interval, _pts, _pt;
    _ymin := infinity;
    _ymax := -infinity;

    # Find de maksimale og minimale y-værdier blandt grafer og punktplots
    for _p in l do
        _data := (op(1, op(1, _p))^+)[2]; # Vector med y-koordianter
        _data := filterUndefined_(_data);
        if min(_data) < _ymin then _ymin := min(_data) end if;
        if max(_data) > _ymax then _ymax := max(_data) end if;
    end do;
    if not pts = FAIL then
        _pts := evalf(pts);         # Udregn 'symbolske' værdier som cos(1)
        # Hvis aksen ikke er tilpasset: Brug std.interval medmindre et punkt falder udenfor
        if _ymin = infinity then 
            _ymin := -10; 
        end if;
        if _ymax = -infinity then
            _ymax := 10;
        end if;
        if type(_pts[1], 'list') then       # liste med punkter
            for _pt in _pts do
                if _pt[2] < _ymin then _ymin := _pt[2] end if;
                if _pt[2] > _ymax then _ymax := _pt[2] end if;
            end do;
        else                                   # enkelt punkt
            if _pts[2] < _ymin then _ymin := _pts[2] end if;
            if _pts[2] > _ymax then _ymax := _pts[2] end if;
        end if;
    end if;
    if _ymax > -infinity then            # Datapunkter bestemmer y-akse
        if _ymin > 0 then _ymin := 0; end if;
        if _ymax < 0 then _ymax := 0; end if;
        _d := _ymax - _ymin;
        _ymin := _ymin - 0.1 * _d;
        _ymax := _ymax + 0.1 * _d;
        _interval := _ymin.._ymax;
    else                    # Brug standard y-akse - kan dette forekomme?
        _interval := -10..10;
    end if;
    _interval;
end proc:

filterUndefined_ := proc(data)
    local l, i;

    l := NULL;
    for i from 1 to op(1, data) do
        if not data[i] = Float(undefined) then
            l := l, data[i];
        end if;
    end do;
    [l];
end proc:


###################
# 
# GetPiecewise_ (args)
#
# Finder de stykkevist definerede funktioner blandt argumenterne.
#
# Returnerer en liste med stykkevist definerede funktioner og en liste med resten
#
GetPiecewise_ := proc(args_::list)
    local piecewise_, rest_, f_;
    piecewise_ := NULL;
    rest_ := [];
    for f_ in args_ do
        if (type(f_, 'procedure') and type(f_('x'), 'piecewise')) or type(f_, 'piecewise') then
            piecewise_ := piecewise_, f_;
        else
            if type(f_, 'procedure') then
                rest_ := [op(rest_), eval(f_)];
            else
                rest_ := [op(rest_), f_];
            end if;
        end if;
    end do;
    [piecewise_], rest_;
end proc;

###################
# 
# UafhængigVar_(funcs)
#
# finder den uafhængige variable - der kan højest være en
# hvis ingen findes så bruges x
#
UafhængigVar_ := proc(funcs::list)
    local tmp_, tmp1_, x_;
    tmp_ := map(x_ -> indets(x_, 'assignable'), funcs);
    tmp1_ := map(x_ -> indets(x_, 'procedure'), funcs);
    tmp_ := `union`(op(tmp_)) minus `union`(op(tmp1_));
    if nops(tmp_) > 1 then
        Err_("Graf: For mange uafhængige variable");
    elif nops(tmp_) = 1 then
        op(tmp_);
    else
        'x';
    end if;
end proc;

PlotStykkevis_ := proc(funcs::list, interval)
    local plots_, punkter_, åbne_punkter_, lukkede_punkter_, f_, i_, part_, ineq_, var_, find_proc_, endepunkter_, lav_punkter_, farve_;

    plots_ := NULL; punkter_ := NULL; åbne_punkter_ := NULL; lukkede_punkter_ := NULL;
    var_ := UafhængigVar_(funcs);
    
    find_proc_ := proc(expr)
        if type(expr, 'procedure') then
            expr(var_);
        else
            expr;
        end;
    end proc;
    
    endepunkter_ := proc(ineq, f)
        local x0_;
        
        if type(op(1, ineq), 'numeric') then # number ineq variable
            x0_ := op(1, ineq);
        else
            x0_ := op(2, ineq);    # variable ineq number
        end if;

        if type(ineq, `<`) then
            punkter_ ,= [x0_, eval(f, var_ = x0_), true];
        else
            punkter_ ,= [x0_, eval(f, var_ = x0_), false];
        end if;
    end proc;
    
    lav_punkter_ := proc(P::list)
        local åbne_, lukkede_, i_;
        
        åbne_ := NULL; lukkede_ := NULL;
        
        for i_ from 1 to nops(P) - 1 do
            if P[i_][1] = P[i_ + 1][1] then
                if P[i_][2] = P[i_ + 1][2] then
                    if P[i_][3] = P[i_ + 1][3] then
                        åbne_ ,= [P[i_, 1], P[i_, 2]];
                    end if;
                    i_ += 1;
                    next;
                end if;
            end if;
            if P[i_][3] then
                åbne_ ,= [P[i_, 1], P[i_, 2]];
            else
                lukkede_ ,= [P[i_, 1], P[i_, 2]];
            end if;
        end do;
        if i_ = nops(P) then
            if P[i_][3] then
                åbne_ ,= [P[i_, 1], P[i_, 2]];
            else
                lukkede_ ,= [P[i_, 1], P[i_, 2]];
            end if;
        end if;
        [åbne_], [lukkede_];
    end proc:

    for f_ in funcs do
        if not type(f_, 'function') then
            f_ := f_(var_);
        end if;
        farve_ := Farve();
        for i_ from 1 by 2 to nops(f_) do
            part_ := find_proc_(op(i_ + 1, f_));    # gren af funktion
            ineq_ := op(i_, f_);
            if type(ineq_, `and`) then
                endepunkter_(op(1, ineq_), part_);
                endepunkter_(op(2, ineq_), part_);
            else
                endepunkter_(ineq_, part_);
            end if;
            plots_ ,= plot(piecewise(ineq_, part_(var_), 'undefined'), var_ = interval, farve_);
        end do;
    end do;
    
    åbne_punkter_, lukkede_punkter_ := lav_punkter_([punkter_]);

    if nops(åbne_punkter_) > 0 then
            plots_ ,= plots:-pointplot(åbne_punkter_, farve_, 'symbol' = 'circle', 'symbolsize' = 15);
    end if;

    if nops(lukkede_punkter_) > 0 then
        plots_ ,= plots:-pointplot(lukkede_punkter_, farve_, 'symbol' = 'solidcircle', 'symbolsize' = 15);
    end if;

    plots_;
end proc;


###################
# 
# Graf(funk1, funk2, ..., datan, [opt, ...])
# 
# Tegner graf for en eller flere funktioner, udtryk eller datamatricer
#  intet argument er påkrævet; rækkefølgen er valgfri
#
#  funki er navnet på en funktion eller et udtyk
#  datai er en 2xn-matrix af x- og y-koordianter for punkter
#  opt kan være
#       'x'=start..stop
#       'y'=start..stop
#       'xakse'='logaritmisk' / 'trig' / antal inddelinger
#       'yakse'='logaritmisk' / 'trig' / antal inddelinger
#       'punkter'=[x, y] eller [[x1, y1], ...]
#       'labels'=[x-lbl, y-lbl]
#       'enhed'=[x-enhed, y-enhed]
#       'område'=f / [f, min..max] / [f, g] / [f, g, min..max]
#       'gitter'
#       'parameter'=min..max
#       'diskontinuert'     ??
#
Graf := proc()
    local _data, _tilvalg, _xinterval, _yinterval, _grafer, _enhed, _labels,
        _f, _points, _item, _områder, _område, _grænser, _gitter, _vektorer, _parameter,
        _xakse, _yakse, _opts, _opt, _piticks, _xvar, _tmp, _tmp1, _stykkevis;
        
    Farve(0); # Start farvevalg fra start.

    _gitter, _tilvalg := FilterOption_('gitter', _passed);
    _stykkevis, _tilvalg := GetPiecewise_(_tilvalg);
    _vektorer, _tilvalg := GetVectors_(_tilvalg);
    _data, _tilvalg := GetType_( Or('procedure','Matrix','algebraic'), _tilvalg );
    _parameter, _tilvalg := GetOption_('parameter', _tilvalg, _def = -10..10);
    _områder, _tilvalg := GetOptions_('område', _tilvalg);
    _points, _tilvalg := GetOption_('punkter', _tilvalg);
    _piticks := ['default','default'];
    if _gitter = 'true' and not HasType_('posint', _opts) then
        _xakse := 'gridlines';
    else
        _xakse := NULL;
    end if;

    _opts, _tilvalg := GetOption_('xakse', _tilvalg);
    if _opts = 'FAIL' then 
        _opts := []; 
    end if;
    for _opt in _opts do
        if type(_opt, 'posint') then
            _xakse ,= 'gridlines'=_opt;
        elif _opt = 'piakse' then
            _piticks[1] := 'piticks';
        elif _opt = 'logaritmisk' then
            _xakse ,= 'mode'='log';
        else
            Err_("Graf: Ukendt tilvalg til 'xakse': mulige tilvalg: ",
                        "et helt tal, 'logaritmisk' og 'trig'");
        end if;
    end do;
    _xakse := ['axis[1]'=[_xakse]];

    _opts, _tilvalg := GetOption_('yakse', _tilvalg);
    if _opts = 'FAIL' then 
        _opts := []; 
    end if;
    _yakse := NULL;
    if _gitter = 'true' and not type(_opts, 'posint') then
        _yakse := 'gridlines';
    end if;
    for _opt in _opts do
        if type(_opt, 'posint') then
            _yakse := 'gridlines'=_opt;
        elif _opt = 'piakse' then
            _piticks[2] := 'piticks';
        elif _opt = 'logaritmisk' then
            _yakse ,= 'mode'='log';
        else
            Err_("Graf: Ukendt tilvalg til 'akse': mulige tilvalg: ",
                        "et helt tal, 'logaritmisk' og 'trig'");
        end if;
    end do;
    _yakse := ['axis[2]'=[_yakse]];
    _xinterval, _tilvalg := GetOption_('x', _tilvalg);
    _yinterval, _tilvalg := GetOption_('y', _tilvalg);
    _enhed, _tilvalg := GetOption_('enhed', _tilvalg);
    if not _enhed = FAIL then
        _labels, _tilvalg := GetOption_('labels', _tilvalg);
        if _labels = FAIL then
            _labels := ['x', 'y'];
        end if;   
        if not _enhed[1] = 1 then
            _labels[1] := sprintf("%a / %a", _labels[1], _enhed[1]);
        end if;
        if not _enhed[2] = 1 then
            _labels[2] := sprintf("%a / %a", _labels[2], _enhed[2]);
        end if;
        _tilvalg := [op(_tilvalg), 'labels' = _labels]
    end if;

    _grafer := NULL; 	# plot funktioner og data

    # Parameterplot af vektorfunktioner og -udtryk
    for _item in _vektorer do
        if type(_item, 'Vector') then
            _xvar := op(indets(_item, 'assignable'));
            _grafer ,= plot([_item[1], _item[2], _xvar = _parameter], 
                                Farve(), op(_tilvalg));
        elif type(_item, 'procedure') then
            _grafer ,= plot([_item(_t)[1], _item(_t)[2], _t = _parameter], 
                                Farve(), op(_tilvalg));
        elif type(_item[1], 'Vector') then
            _xvar := op(indets(_item[1], 'assignable'));
            _grafer ,= plot([_item[1][1], _item[1][2], _xvar = _item[2]], 
                                Farve(), op(_tilvalg));
        else
            _grafer ,= plot([_item[1](_t)[1], _item[1](_t)[2], _t = _item[2]], 
                                Farve(), op(_tilvalg));
        end if;
    end do;
    
    if _xinterval = FAIL then
        _xinterval := XInterval_(_data, _points, [_grafer]);
    end if;

    _grafer ,= PlotStykkevis_(_stykkevis, _xinterval);

    # Bestem uafhængig variable
    _tmp := map(_x -> indets(_x, 'assignable'), _data); 
    _tmp1 := map(_x -> indets(_x, 'procedure'), _data);
    _tmp := `union`(op(_tmp)) minus `union`(op(_tmp1));
    if nops(_tmp) > 1 then
        Err_("Graf: For mange uafhængige variable");
    elif nops(_tmp) = 1 then
        _xvar := op(_tmp);
    else
        _xvar := x;
    end if;

    # Plot funktioner, udtryk og datasæt
    for _item in _data do
        if type(_item, 'Matrix') then
            _grafer ,= plots:-pointplot(_item, Farve(), op(_tilvalg));
        else
            # funktionsnavn eller x->...
            if type(_item, 'procedure') then
                _f := _item;
                
            # udtryk - f(x) eller blot algebraisk udtryk
            else
                _f := unapply(_item, _xvar);
            end if;
            
            if not _enhed = FAIL then
                _f := unapply(simplify(
                        _f(x * Unit(_enhed[1])) / Unit(_enhed[2])), _xvar);
            end if;
            _grafer ,= plot(_f, _xinterval, Farve(), op(_tilvalg), 'discont');
        end if;
    end do;

    # Plot enkeltpunkter
    if not _points = FAIL then	# plot punkter
        _grafer ,= plots:-pointplot(_points, Farve(), op(_tilvalg));
    end if;

    if _yinterval = FAIL then	# find aksebegrænsninger
        _yinterval := YInterval_([_grafer], _points);
    end if;

    # Find afgrænsning af område
    # sørg for at ligninger kun har reelle løsninger
    use plots, RealDomain in           
    for _område in _områder do
        # 'område'=f
        if type(_område, 'procedure') then
            # brug mængde for at undgå gentagne løsninger
            _grænser := {solve(_område(_xvar) = 0)};    
            if not nops(_grænser) = 2 then
                Err_("Graf: Funktionen %1 har ikke to rødder - ",
                            "angiv interval for arealet", _område(_xvar));
            end if;
            _grafer ,= inequal(
                [
                    'y' >= 0, 'y' <= _område('x'), 
                    'x' >= min(_grænser), 'x' <= max(_grænser)
                ],
                'x' = _xinterval, 'y' = _yinterval, 'nolines' 
            );
        # 'område'=[f,g]
        elif type(_område, list(procedure)) and nops(_område) = 2 then
            _grænser := {solve(_område[1](_xvar) = _område[2](_xvar))};
            if not nops(_grænser) = 2 then
                Err_("Graf: Ligningen %1 har ikke to løsninger - ",
                            "angiv interval for arealet",
                    _område[1](_xvar) = _område[2](_xvar));
            end if;
            _grafer ,= inequal(
                [
                    'y' >= _område[2]('x'), 'y' <= _område[1]('x'), 
                    'x' >= min(_grænser), 'x' <= max(_grænser)
                ],
                'x' = _xinterval, 'y' = _yinterval, 'nolines'
            );
        # 'område'=[f,a..b]
        elif type(_område, 'list') and nops(_område) = 2 then                
            if not type(_område[1], 'procedure') 
              or not type(_område[2], `..`) then
                Err_("Graf: Fejl i angivelse af område: område=%1 - ",
                            "brug ?Graf for at få hjælp", _område);
            end if;
            _grafer ,= inequal(
                [
                    'y' >= 0, 'y' <= _område[1]('x'),
                    'x' >= op(1, _område[2]), 'x' <= op(2, _område[2])
                ],
                'x' = _xinterval, 'y' = _yinterval, 'nolines' 
            );
        # 'område'=[f,g,a..b]
        elif type(_område, 'list') and nops(_område) = 3 then
            if not type(_område[1..2], 'list(procedure)') 
              or not type(_område[3], `..`) then
                Err_("Graf: Fejl i angivelse af område: område=%1 - ",
                            "brug ?Graf for at få hjælp", _område);
            end if;
            _grafer ,= inequal(
                [
                    'y' >= _område[2]('x'), y <= _område[1]('x'), 
                    'x' >= op(1, _område[3]), 'x' <= op(2, _område[3])
                ],
                'x' = _xinterval, 'y' = _yinterval, 'nolines' 
            );
        end if;
    end do;
    end use;

    plots:-display(_grafer, 'view'=[_xinterval,_yinterval], op(_xakse), op(_yakse), 
            'tickmarks'=_piticks);
end proc;

###################
# Tangent(f, x0 [, 'graf' [, tilvalg]])
#
# Bestemmer tangenten i et punkt eller laver graf med funktion og tangent
#
# - f er en funktion eller et funktionsudtryk med x som variabel
# - x0 er røringspunktets x-koordinat
# - 'graf' : tilvalg som angiver at grafen skal tegnes (valgfrit)
# - 'vis' : tilvalg som angiver at ligningen skal printes (valgfrit)
# - tilvalg : yderligere tilvalg (se Graf)
#
Tangent := proc(f, x0)
    local _f, _fm, _t, _tilvalg, _opt;
    
    # f er navnet på en defineret funktion eller x->...
    if type(f, 'procedure') then
        _f := f;
    # f er et algebraisk udtryk eller f(x) (eller kombination heraf)
    elif type(f, 'algebraic') then
        _f := unapply(f, x);
    end if;
    
    _fm := unapply(diff(_f(x), x), x); # f'
    _t := unapply(_fm(x0)*(x - x0) + _f(x0), x); # tangenten
    
    if HasOption_('vis', _rest) then
        Print_(Line_(y=collect(expand(_t(x)),x)));
    end if;
    
    # grafen tegnes kun, hvis plot-kommandoen er det sidste i proceduren
    if HasOption_('graf', _rest) then
        _opt, _tilvalg := GetOption_('graf', _rest);
        Graf(_f, _t, op(_tilvalg));
    else
        eval(_t);
    end if;
end proc;
    
###################
Gradient := proc(f::Or(procedure, algebraic))
    local x_, y_, res_;
    x_, y_ := op(FindOption_('variable', _rest, _def=['x','y']));
    if type(f, 'procedure') then
        res_ := <diff(f(x_, y_),x_),diff(f(x_, y_),y_)>;
        Print_(Line_(Nabla, f, "(",  x_, ", ", y_, ") = ", res_));
    else
        res_ := <diff(f, x_), diff(f, y_)>;
        Print_(Line_(Nabla, "(", f, ") = ", res_));
    end if;
    unapply(res_, x_, y_);
end proc;

###################
# Linjeelementer(F [, opt]...)
#
# Tegn Linjeelementer for differentialligningen y' = F(x,y)
#
# - F er en funktion eller et udtryk i to variable (def. x og y)
# - opts:
#   - x  : range. interval på 1.-aksen (def. -10..10)
#   - y  : range. interval på 2.-aksen (def. -10..10)
#   - vars : liste med navne på variable (def. [x, y])
#   - funktioner : liste med funktionsudtryk
#   - punkter : liste med punkter
#
Linjeelementer := proc(F::Or(procedure, algebraic))
    local x_range_, y_range_, vars_, p_, F_, fs_, f_, ps_, tilvalg_;
    
    x_range_, tilvalg_    := GetOption_('x', _rest, '_def' = -10..10);
    y_range_, tilvalg_    := GetOption_('y', op(tilvalg_), '_def' = -10..10);
    vars_, tilvalg_ := GetOption_('vars', op(tilvalg_), '_def' = ['x', 'y']);
    fs_, tilvalg_   := GetOption_('funktioner', op(tilvalg_), '_def'=[]);
    ps_, tilvalg_   := GetOption_('punkter', op(tilvalg_));

    # fieldplot accepterer kun x og y som variable
    if type(F, 'procedure') then
        F_ := F('x', 'y');
    else
        F_ := subs([vars_[1]='x', vars_[2]='y'], F);
    end if;

    p_ := plots:-fieldplot([1, F_], 'x'=x_range_, 'y'=y_range_, 'arrows'='line', 
                                'fieldstrength'='fixed', Farve(1));
    for f_ in fs_ do
        p_ ,= plot(f_, 'x'=x_range_, Farve(), 'discont');
    end do;

    if not ps_ = FAIL then	# plot punkter
        p_ ,= plots:-pointplot(ps_, Farve(), 'symbol'='solidcircle', op(tilvalg_));
    end if;
    
    plots:-display(p_, 'view'=[x_range_, y_range_], op(tilvalg_));
end proc;

###################
# Graf3d(f, [, opts])
#
# Tegner en 3d-graf for en funktion, f(x,y)
#
# - f er en funktion i x og y
# - opts:
#   - punkter : liste med par [x, y] (eller blot et enkelt par)
#   - tangentplan : do.
#   - xsnitkurver : y-værdi eller liste med y-værdier
#   - ysnitkurver : x-værdi eller liste med x-værdier
#   - niveaukurver : ingen værdi, antal kurver eller liste med kurveværdier
#   - x : interval (def -10..10)
#   - y : interval (def -10..10)
#   - z : interval
#   - boks: -
#   - gitter: -
#
Graf3d := proc(f::Or(procedure, algebraic))
use plots, VectorCalculus in
    local xs_, ys_, zs_, view_, f_, tilvalg_, plots_, punkter_, kontur_, snit_, fx_, fy_, x0_, y0_, p_;
    
    tilvalg_ := NULL; plots_ := NULL;
    
    if type(f, 'procedure') then
        f_ := f;
    else
        f_ := unapply(f, x, y);
    end if;
    
    if not HasOption_('boks', _rest) then
        tilvalg_ ,= 'axes'='normal';
    end if;
    if not HasOption_('gitter', _rest) then
        tilvalg_ ,= 'style'='patchnogrid';
    end if;
    xs_ := FindOption_('x', _def = -10..10, _rest);
    ys_ := FindOption_('y', _def = -10..10, _rest);
    zs_ := FindOption_('z', _rest);
    if zs_ = FAIL then
        view_ := NULL;
    else
        view_ := view = [xs_, ys_, zs_];
    end if;
   
    # plot punkter
    if HasOption_('punkter', _rest) then
        punkter_ := FindOption_('punkter', _rest);
        if not type(punkter_[1], list) then
            punkter_ := [punkter_];
        end if;
        punkter_ := map(p -> [p[1], p[2], eval(f_(p[1], p[2]))], punkter_);
        plots_ ,= pointplot3d(punkter_, 'symbol'='solidsphere', 'color'='black');
    end if;
    
    # plot 3d-graf
    if HasOption_('niveaukurver', _rest) then
        kontur_ := FindOption_('niveaukurver', _rest);
        if kontur_ = FAIL then
            kontur_ := NULL;
        else
            kontur_ := 'contours'=kontur_;
        end if;
        plots_ := contourplot3d(f_(x,y), 'x'=xs_, 'y'=ys_, 'filledregions'=true, kontur_), plots_;
    else
        plots_ := plot3d(f_(x,y), 'x'=xs_, 'y'=ys_, tilvalg_), plots_;
    end if;
    
    # x-snitkurver
    if HasOption_('xsnitkurver', _rest) then
        snit_ := FindOption_('xsnitkurver', _rest);
        if not type(snit_, list) then
            snit_ := [snit_];
        end if;
        for y0_ in snit_ do
            plots_ ,= SpaceCurve(<'x', y0_, f_('x', y0_)>, 'x'=xs_, 'color'='black');
        end do;
    end if;
    
    # y-snitkurve
    if HasOption_('ysnitkurver', _rest) then
        snit_ := FindOption_('ysnitkurver', _rest);
        if not type(snit_, list) then
            snit_ := [snit_];
        end if;
        for x0_ in snit_ do
            plots_ ,= SpaceCurve(<x0_, 'y', f_(x0_, 'y')>, 'y'=ys_, 'color'='black');
        end do;
    end if;
    
    # tangentplaner
    if HasOption_('tangentplan', _rest) then
        punkter_ := FindOption_('tangentplan', _rest);
        if not type(punkter_[1], 'list') then
            punkter_ := [punkter_];
        end if;
        fx_ := PartielAfledet(f_, 'x');
        fy_ := PartielAfledet(f_, 'y');
        for p_ in punkter_ do
            plots_ ,= plot3d(
                f_(op(p_)) + ('x' - p_[1])*fx_(op(p_)) + ('y' - p_[2])*fy_(op(p_)), 'x'=xs_, 'y'=ys_
            );
        end do;
    end if;
    
    display(plots_, view_);
    
end use
end proc;
