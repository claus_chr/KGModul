#############################################################################
#                                                                           #
# Copyright (C) 2018-19 by Claus Christensen                                #
# This file is part of KGModul                                              #
#                                                                           #
# This program is free software: you can redistribute it and/or modify      #
# it under the terms of the GNU General Public License as published by      #
# the Free Software Foundation, either version 3 of the License, or         #
# (at your option) any later version.                                       #
#                                                                           #
# This program is distributed in the hope that it will be useful,           #
# but WITHOUT ANY WARRANTY; without even the implied warranty of            #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
# GNU General Public License for more details.                              #
#                                                                           #
# You should have received a copy of the GNU General Public License         #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    #
#                                                                           #
#############################################################################

KGModul := module()

    export 
        Info, Init, 
        Vis, Løs, Funktionstabel, Gem, Hent, LøsDiff, Afledet, Stamfunktion, PartielAfledet,
        Graf, Farve, Tangent, Gradient, Linjeelementer, Graf3d,
        Cos, Sin, Tan, InvCos, InvSin, InvTan, tilGrad, tilRad,
        LineærModel, EksponentielModel, PotensModel, ProportionelModel, 
            KvadratiskModel, ReciprokModel, AndengradsModel,
            PolynomiumsModel, Residual, Rkvadrat, IndlæsExcelData,
        AntalObservationer, LavTabel, Stolpediagram, Prikdiagram, Trappediagram, Histogram, Sumkurve, 
            Fraktil, Kvartilsæt, Boksplot, Boxplot, Middelværdi, Varians, 
            Standardafvigelse, Spredning, Typetal, Typeinterval, Deskriptorer,
        BinomP, BinomK, NormalP, NormalK, Chi2P, Chi2K, Chi2Fit, Chi2Uafhængighed, FraktilPlot,
        Konstant, Isotopmasse, GemData, HentData, Molarmasse,
        Tværvektor, Skalarprodukt, Længde, Determinant, Vektorprodukt, Projektion, 
            Afstand,

        PakkeNavn_, Version_, Date_, Copyright_,
        Fejl, Err_, isFloat, AfrundTal_, Afrund_, SaveName_, Cifre_, Some_, None_, All_,
        Line_, TextLines_, Print_, Plot_,
        GetOption_, GetOptions_, FindOption_, HasOption_, FilterOption_, FilterOptions_,
            GetType_, HasType_, GetVectors_, FindValues_, FindOptions_,
        Saml_, Sorterløsning_, FindUbekendte_, ParseLin_, ParseEksp_, erPolynomiel_,
        XInterval_, YInterval_, filterUndefined_, farve_, GetPiecewise_, PlotStykkevis_, UafhængigVar_,
        PrintFunk_, Model_, TilpasMatrix_, Interval_,
        TjekData_, TilpasData_, Antal_, Akser_, Optæl_, OptælBin_, GetMinimum_, Varians_, 
            FindFraktil_, LavInterval_, LavObservationer_, LavRække_, FindMatrix_,
        Value_, Chi2Out_,
        Molarmasse_,
        MakeVector_, distPP_, distPl_, distll_, distPa_, repr_,
        
        test
    ;

    option package;
    
    PakkeNavn_ := "KGModul";
    Version_ := "---";
    Date_ := "---";
    Copyright_ := "Copyright (C) 2011 Claus Christensen";
    

# Fejl, Err_, AfrundTal_, Afrund_, SaveName_, Cifre_
#
$include "src/support.mm"           

# Line_, TextLines_, Print_, Plot_
#
$include "src/output.mm"            

# GetOption_, GetOptions_, FindOption_, HasOption_, FilterOption_, FilterOptions_, 
# GetType_, HasType_, GetVectors_, FindValues_, FindOptions_
#
$include "src/argumentHandling.mm"  

# Info, Init 
#
$include "src/Init.mm"              

# Vis, Løs, Funktionstabel, Gem, Hent, LøsDiff, Afledet, Stamfunktion, PartielAfledet,
# Saml_,  Sorterløsning_, FindUbekendte_, 
# ParseLin_, ParseEksp_, erPolynomiel_
#
$include "src/GeneralUtils.mm"      

# Graf, Farve, Tangent, XInterval_, YInterval_, farver_
#
$include "src/Tegn.mm"              

# Cos, Sin, Tan, InvCos, InvSin, InvTan, tilGrad, tilRad
$include "src/Trigonometri.mm"      

# LineærModel, EksponentielModel, PotensModel, ProportionelModel, KvadratiskModel, 
# ReciprokModel, AndengradsModel, PolynomiumsModel, Residual, Rkvadrat,
# PrintFunk_, Model_, TilpasMatrix_
#
$include "src/Regression.mm"        

# AntalObservationer, LavTabel, Stolpediagram, Trappediagram, Histogram, Sumkurve, 
# Fraktil, Kvartilsæt, Boksplot, Boxplot, Middelværdi, Varians, Standardafvigelse, 
# Typetal, Typeinterval, Deskriptorer, TjekData_, 
# TilpasData_, Antal_, Akser_, Optæl_, GetMinimum_, Varians_, FindFraktil_
# LavInterval_, LavObservationer_, LavRække_, FindMatrix_
#
$include "src/DeskriptivStatistik.mm" 

# BinomP, BinomK, NormalP, NormalK, Chi2P, Chi2K, Chi2Fit, Chi2Uafhængighed, Value_, 
# Chi2Out_
#
$include "src/Statistik.mm"         

# Konstant, Isotopmasse, GemData, HentData, Molarmasse, Molarmasse_
#
$include "src/Science.mm"           

# Tværvektor, Skalarprodukt, Længde, Determinant, Vektorprodukt, Projektion, Afstand
# MakeVector_, distPP_, distPl_, distll_, distPa_, repr_
#
$include "src/Geometri.mm"

# Test
# test
$include "test/test.mm"


end module;
