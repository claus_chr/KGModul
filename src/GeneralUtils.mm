#############################################################################
#                                                                           #
# Copyright (C) 2018-19 by Claus Christensen                                #
# This file is part of KGModul                                              #
#                                                                           #
# This program is free software: you can redistribute it and/or modify      #
# it under the terms of the GNU General Public License as published by      #
# the Free Software Foundation, either version 3 of the License, or         #
# (at your option) any later version.                                       #
#                                                                           #
# This program is distributed in the hope that it will be useful,           #
# but WITHOUT ANY WARRANTY; without even the implied warranty of            #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
# GNU General Public License for more details.                              #
#                                                                           #
# You should have received a copy of the GNU General Public License         #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    #
#                                                                           #
#############################################################################

###################
# Saml_('saml' [=var])
#
# Samler led med samme potens af var, def = x. Bruges af vis
# 
Saml_ := proc(værdi)
    local _saml;
    if HasOption_('saml', _rest) then
        _saml := FindOption_('saml', _rest, '_def'='x');
        return collect(værdi, _saml);
    else
        return værdi;
    end if;
end proc;



###################
# Sorterløsning_(L)
#
# Sorterer en liste med løsninger efter værdien af løsningen
# Løsninger med flere variable sorteres efter den første variabel
# Bruges af Løs.
#
# - L er en liste med løsninger; hver løsning er en liste af ligninger el. uligheder
#
Sorterløsning_ := proc(L)
    local rank_, lst_, L_, kompleks_, variabel_, tal_, ulighed_;
    # Sorter komplekse løsningre fra
    kompleks_ := proc(lst) 
        Some_(x -> type(evalf(x), 'nonreal'), map(rhs, lst));
    end proc;
    L_ := remove(kompleks_, L);
    
    # Lister med løsninger, som indeholder flere variable kan ikke ordnes numerisk
    variabel_ := proc(lst)
        Some_(eq -> nops(indets(eq)) > 1 or rhs(eq) = lhs(eq), lst);
    end proc;
    if Some_(variabel_, L_) then 
        return L_; 
    end if;
    
    tal_ := proc(eq) # tallet i lign.
        if nops(indets(rhs(eq))) = 1 then
            lhs(eq);
        else
            rhs(eq); # i uligheder kan den variable optræde på højre side
        end if;
    end proc;
    
    # Sorter løsninger til dobbeltuligheder
    ulighed_ := proc(lst)
        lst_ := map(tal_, lst);
        rank_ := Statistics:-Rank(lst_);
        Statistics:-OrderByRank(lst, rank_);
    end proc;
    if nops(indets(L_)) = 1 then
        L_ := map(ulighed_, L_);
    end if;

    lst_ := map(tal_, map(l -> l[1], L_)); # tallet i 1. lign. i hver liste
    rank_ := Statistics:-Rank(lst_);
    Statistics:-OrderByRank(L_, rank_);
end proc;
    
    
###################
# ParseLin_(udtryk)
#
# Finder koefficienter og den variable i et lineært udtryk. Bruges af ParseEksp_
#
# Tager et udtryk af formen a*x+b og returnerer listen [b, a, x]
#
ParseLin_ := proc(u)
    local _a, _b, _x, _temp;
    
    if type(u, `+`) then
        _b := op(2, u);
        _temp := op(1, u);
    else
        _b := 0;
        _temp := u;
    end if;
    if type(_temp, `*`) then
        _a := op(1, _temp);
        _x := op(2, _temp);
    else
        _a := 1;
        _x := _temp;
    end if;
    _b, _a, _x;
end proc;


###################
# ParseEksp_(udtryk)
#
# Finder koefficienter og den variable i et eksponentielt udtryk. Bruges af Vis
#
# Eksponentielle udtryk er følsomme overfor afrunding, så Maples metode med at
# afrunde deludtryk først kan give ret unøjagtige resultater.
#
# Returnerer [b, k, x] eller NULL, hvis udtrykket ikke er eksponentielt
#
ParseEksp_ := proc(u)
    local u_;
    
    u_ := simplify(evalf[100](u));

    if op(0, u_) = 'exp' then
        return [1.0, op(op(u_))];
    elif nops(u_) = 2 and op(0, op(2, u_)) = 'exp' then
        return [op(1, u_), op(op(op(2, u_)))];
    else
        return NULL;
    end if;
end proc;


###################
# Vis(udtryk, [opts, ...])
#
# Samler led med samme potens af var, def = x. Bruges af vis
#
# Viser værdien af et udtryk som 'udtryk = værdi'
#
# udtryk er et Maple-udtryk, en variabel eller en funktionsværdi
#
# options:
#   cifre = <n> : n er et positivt helt tal, som angiver antallet af 
#       betydende cifre eller evt. 0, hvis der ikke skal regnes numerisk;
#   enhed = <u> : u er den enhed (angivet som tekst) som resultates skal vises i
#   saml [= <var>] : Samler led med samme potens af var, def = x. 
#   procent, grader, punkt, naturlig: speciel formattering af resultatet
#   temperatur: resultat er en temperatur ikke en tilvækst
#
Vis := proc(udtryk::uneval)
    local _decimaler, _værdi, _enhed, _pct, _tmp, _f, _n, _x0, _a, _b, _k, _x, _eksp;

    _pct := HasOption_('procent', _rest);
    _tmp := HasOption_('temperatur', _rest);

        # Tjek for eksponentielt udtryk før afrunding og forenkling
    _eksp := ParseEksp_(eval(udtryk));

    if _tmp then
        # simplify omregner C til K, så undgå den
        _værdi := eval(udtryk);
    else
        _værdi := simplify(eval(udtryk));   # Udregn udtrykket
    end if;
    # procenttal multipliceres før afrunding; ellers får det afrundede 
    # resultat tilføjet 2 nuller
    if _pct then 
        _værdi := 100 * _værdi;
    end if;
    
    _enhed := FindOption_('enhed', _rest);
    _decimaler := -1;
    
    if HasOption_('cifre', _rest) then
        _decimaler := FindOption_('cifre', _rest);
        if _decimaler = 0 then
            _decimaler := Digits;
        end if;
    end if;
    if not _enhed = FAIL then
        if _decimaler = -1 then  # tal med enhed afrundes altid
            _decimaler := Digits;
        end if;
    end if;
    if _decimaler = -1 and (_pct or HasOption_('grader', _rest)) then
        # procent- og gradtal afrundes altid
        _decimaler := Digits;
    end if;
    if _decimaler > 0 then
        # Afrund til det ønskede antal decimaler
        if _tmp then
            _værdi := Afrund_(_værdi, _decimaler);  # simplify omregner C til K, så undgå den
        else
            _værdi := simplify(Afrund_(_værdi, _decimaler));
        end if;
        # potens- og eksponentialudtryk kræver særlig behandling
        if op(0, _værdi) = `^` or op(0, _værdi) = 'exp' then
            _værdi := 1.0 * _værdi;
        end if;
        if op(0, op(2, _værdi)) = `^` 
          and type(op(1, op(2, _værdi)), 'symbol') then 
            # eksponenten skal afrundes særskilt
            _a := Afrund_(op(2, op(2, _værdi)), _decimaler); 
            _b := op(1, _værdi);
            _x := op(1, op(2, _værdi));
            _værdi := _b * _x^_a;           
        elif not _eksp = NULL then
            # Eksponentielle funktioner udtrykkes som e^kx - omregn til a^x
            _b := Afrund_(_eksp[1], _decimaler);
            _k := _eksp[2];
            _x := _eksp[3];
            if HasOption_('naturlig', _rest) then
                _k := Afrund_(_k, _decimaler);
                _værdi := _b * exp(_k * _x);
            else
                _a := Afrund_(exp(_k), _decimaler);
                _værdi := _b * _a^_x;
            end if;
        end if;
    end if;
    if not _enhed = FAIL then   # Omregn til ønsket enhed
        if _tmp then
            _værdi := convert(_værdi, 'temperature', _enhed);
        else
            _værdi := convert(_værdi, 'units', _enhed);
        end if;
        _værdi := Afrund_(_værdi, _decimaler);
    end if;

    if _pct then
        udtryk = nprintf("%.*g%%", _decimaler, _værdi);
    elif HasOption_('grader', _rest) then
        udtryk = nprintf("%.*g%s", _decimaler, _værdi, "°");
    elif HasOption_('punkt', _rest) then
        local i;
        convert(udtryk, 'symbol')(seq(_værdi[i], i=1..op(1, _værdi)));
    elif type(udtryk, 'function') and 
     (op(0, udtryk) in {'eval', 'diff'} or op(0, op(0, udtryk)) = 'D') then    
        if op(0, udtryk) = 'eval' then #    f'(x0) og f'(var) når var != x
            _f := convert(op(1, op(1, udtryk)), 'string'); # f(x)
            _x0 := rhs(op(2, udtryk));
        elif op(0, udtryk) = 'diff' then    # f'(x)
            _f := convert(op(1, udtryk), 'string');
            _x0 := op(2, udtryk);
        elif op(0, op(0, udtryk)) = 'D' then    # D(..)(..) - fx ved O'(...)
            _f := cat(convert(op(op(0, udtryk)), 'string'), "(");
            _x0 := op(1, udtryk);
        end if;
        _n := StringTools:-FirstFromLeft("(", _f) - 1;
        _f := _f[1.._n];
        if StringTools:-FirstFromLeft("_", _f) > 0 then
            _f := convert(_f, name);
            Print_(Line_(_f, "'(", _x0, ") = ", Saml_(_værdi, _rest)));
        else
            _x0 := convert(_x0, 'string');
            if _f = "f" then _f := "f "; end if;    # ellers kan man ikke se '
            _f := cat(_f, "'(", _x0, ")");                      # f'(x0)
            convert(_f, 'name') = Saml_(_værdi, _rest);
        end if;
    else
        udtryk = Saml_(_værdi, _rest);
    end if;
end proc;

###################
# FindUbekendte_(lign)
#
# Find de ubekendte i en sekvens af ligninger
#
# - lign er en sekvens af 1 eller flere ligninger og uligheder
#
# Returnerer en liste med de ubekendte
#
FindUbekendte_ := proc()
    local _xs, _lign, _l, _find;
    _find := eq -> map(x->convert(x, 'unit_free'), eq);
    
    _xs := {};
    for _lign in _rest do
        # fjern enheder, da de opfattes som variable. Håndter hver side af ligningen for sig,
        # da det ellers ikke virker i visse tilfælde (fx ved polynomiale udtryk)
        _l := _find(lhs(_lign)) = _find(rhs(_lign));
        
        # 'assignable' sørger for at symboler som pi og sin ikke medtages
        _xs := _xs union indets(_l, 'assignable');
    end do;
    convert( _xs, 'list');
end proc;

###################
# erPolynomiel_(lign)
#
# sand, hvis alle ligninger i lign er polynomielle; ellers falsk
#
erPolynomiel_ := proc(lign::seq(Or(equation, `<`, `<=`)))
    local polynomiel_, lign_;
    polynomiel_ := true;
    for lign_ in [lign] do
        if type(lign_, equation) and not type(rhs(lign_) - lhs(lign_), polynom) then
            polynomiel_ := false;
        end if;
    end do;
    polynomiel_;
end proc;

###################
# Løs(lign [, opts])
#
# Løs ligning eller system af ligninger. Udskriv resultater pænt!
# 
# - lign er en sekvens af 1 eller flere ligninger og uligheder
# - opts kan være:
#       'cifre'=n       hvor n er antal af betydende cifre i løsningerne
#       'interval'=int  hvor int er et interval, som løsningerne skal tilhøre
#                       som en liste eller range (start..stop)
#       'antal'=n       hvor n er det maksimale antal løsninger, der skal findes
#                       Kun for ponynomielle lign.
#       'numerisk'      angiver at der ønskes en numerisk løsning
#       en variabel eller liste med variable, der skal løses med hensyn til
#
# Returnerer en sekvens af løsninger, medmindre der er flere variable og 
# løsninger. Løsninger afrundes ikke selv om de udskrives i afrundet form.
#
Løs := proc( 
    lign::seq(Or(equation, `<`, `<=`, `and`)), 
    {cifre::integer := -1, interval := NULL, antal := NULL} 
)
    local _flatten, _og, _eller, _Og, _Eller, _udpak, _opt, _sol, _l, 
          _L, opt_, _solv, interval_, _numerisk, _rst, _lign, _dim, _locals,
          antal_, dummy_, warning_, tilvalg_;
    
    # Hjælpefunktioner
    _flatten := _l -> if type(_l, 'list') then op(_l); else _l; end if;
    
    _og := (_x, _y) -> [_flatten(_x), " og ", _y];
    
    _eller := (_x, _y) -> [_flatten(_x), " eller ", _flatten(_y)];
    
    _Og := _l -> foldl(_og, op(_l));
    
    _Eller := proc(_l)
        local l_;
        l_ := foldl(_eller, op(map(_Og, _l)));
        if type(l_, 'list') then op(l_); else l_; end if;
    end proc;
    
    _udpak := _l -> map(l_ -> map(_x -> rhs(_x), l_), _l);
    
    warning_ := NULL;
    if HasOption_('numerisk', _rest) and not HasOption_('interval', _rest)
        and not erPolynomiel_(lign) then
            if not antal = 1 then
                warning_ := TextLines_(
                    "Advarsel: Du bør både angive et interval og et maksimalt antal løsninger;",
                    "ellers findes der højest én løsning, uanset hvor mange løsninger der er."
                );
                antal_ := 1;
            end if;
    else
        if antal = NULL then
            antal_ := 5;
        else
            antal_ := antal;
        end if;
    end if;

    _numerisk, _rst := FilterOption_('numerisk', _rest);
    
    # omform dobbeltulighed til to uligheder og vektorligninger til ligninger for hver komponent
    _lign := NULL;  
    for _l in [lign] do
        if type(_l, `and`) then     # dobbeltulighed
            _lign ,= op(_l);
        elif type(lhs(_l), 'Vector') then   # vektorligning
            _dim := LinearAlgebra:-Dimension(lhs(_l));
            local i;
            _lign ,= seq(lhs(_l)[i] = rhs(_l)[i], i=1.._dim);
        else
            # Workaround for erf: hverken solve eller fsolve kan løse ligninger med erf,
            # hvis udtrykket skal være lig et lille decimaltal
            #
            if has(_l, erf) then
                if type(rhs(_l), float) then
                    _lign ,= lhs(_l) = convert(rhs(_l), rational);
                elif type(lhs(_l), float) then
                    _lign ,= convert(lhs(_l), rational) = rhs(_l);
                else
                    _lign ,= _l;
                end if;
            else
                _lign ,= _l;
            end if;
        end if;
    end do;
    
    if not _rst = [] then # find de ubekendte
        # ...angivet som symbol eller liste/mængde af symboler
        _opt := convert(_rst[1], 'list');	
    else
        _opt := FindUbekendte_(_lign);
    end if;

    # Løs ligningerne  => [[var=løsn,...],...]
    _locals := map( x->convert(x, '`local`'), _opt);
    opt_ := zip( (x,y)->(x = y), _opt, _locals);
    _opt := _locals; 
    _L := subs(opt_, [_lign]);
    if interval = NULL and not _numerisk then
        tilvalg_ := NULL;
        if not erPolynomiel_(op(_L)) then
            # 'explicit' er nødvendig for at få alle konkrete løsninger til 
            # trigonometriske ligninger i en interval
            tilvalg_ ,= 'allsolutions', 'explicit';
        end if;
        _sol := RealDomain:-solve(_L, _locals, tilvalg_);
            # simple polynomielle ligninger m. flere variable udtrykkes vha. RootOf
            # L = [[var = RootOf..., ...]]
        if nops(_sol) = 1 and Some_(x -> type(x, RootOf), map(rhs, op(_sol))) then
            _sol := [allvalues(op(_sol))];
        end if;
    elif not interval = NULL then
        if type(interval, 'list') then
            interval_ := interval[1]..interval[2];
        elif type(interval, `..`) then
            interval_ := interval;
        else
            error(cat(
                "Fejl i intervalangivelse. Brug \"interval=min..max\" ",
                "eller \"interval=[min,max]\""
            ));
        end if;
        _sol := [    # liste af mængder af løsninger
            fsolve(_L, convert(_locals[1], 'set'), interval_, 'maxsols'=antal_)
        ]; 
        # konverter til liste af lister af løsninger
        _sol := map(item -> convert(item, 'list'), _sol);
    else 
        # vi kommer kun her ved numerisk løsning uden interval 
        # - kun én løsning med mindre ligning er polynomiel
        _sol := [    # liste af mængder af løsninger
            fsolve(_L, convert(_locals[1], 'set'), 'maxsols'=antal_)
        ]; 
        # konverter til liste af lister af løsninger
        _sol := map(item -> convert(item, 'list'), _sol);
    end if;
    _sol := Sorterløsning_(_sol);

    if _sol = [] then
        Print_(warning_, TextLines_("Ligningen har ingen løsning"));
    else
        _solv := _udpak(_sol); # omform løsn. til kun at omfatte tal
        _sol := Afrund_(_sol, '_cifre'=cifre);
        Print_(warning_, Line_(_Eller(_sol)));

        # returner løsninger uden 'var =', så de kan tildeles variable
        
        # Kun en ubekendt
        if nops(_opt) = 1 then
            op(map(l -> op(l), _solv));     #   => sekvens  af løsninger
            
        # Flere ubekendte med kun en løsninger
        elif nops(_solv) = 1 then
            op(op(_solv));                  #   => sekvens af løsninger
            
        else
            
            op(_solv);                      #   => liste af løsningssæt
        end if;
    end if;
end proc;
    
    
###################
# Funktionstabel(f, x [, spring] [, opts...])
#
# Laver en tabel med funktionsværdier
# - f er en funktion eller et udtryk i en variabel
# - x er et interval eller en liste eller vektor med x-værdier
# - spring angiver afstanden imellem succesive x'er - kun for intervaller
#       (def = 1)
# - cifre: afrunding (def = 4)
# - tilvalg: navne=[første, anden] to strenge som printes i 1. søjle
#
Funktionstabel := proc(
    f::Or(procedure, algebraic), x::Or(range, Vector, list)
)
    local _d, _n, _x1, _x2, _xs, _ys, _T, _round, _i, _navne, _x_navn, _y_navn, _var, _f;

    if _rest = NULL or type(_rest[1], `=`) then
        _d := 1;
    else
        _d := _rest[1];
    end if;
    _round := Cifre_(_rest, '_def' = 4);
    _navne := FindOption_('navne', _rest, '_def' = ["x", "y"]);
    _x_navn := _navne[1];
    _y_navn := _navne[2];
    
    if type(x, 'range') then
        _x1 := op(x)[1];
        _x2 := op(x)[2];
        _n := ceil((_x2 - _x1) / _d);
        _xs := [seq(_x1 + _i * _d, _i = 0.._n-1), _x2];
    else
        if type(x, 'Vector') then
            _xs := convert(x, 'list');
        else
            _xs := x;
        end if;
        _n := nops(_xs) - 1;
    end if;

    if type(f, procedure) then
        _f := f;
    else
        _var := indets(f)[1];
        _f := unapply(f, _var);
    end if;

    _ys := map(_f, _xs);
    
    use DocumentTools:-Layout in
        for _i from 1 to _n + 1 do
            if not type(_xs[_i], 'integer') then
                _xs[_i] := Equation(Afrund_(_xs[_i], _round));
            end if;
            if not type(_ys[_i], integer) then
                _ys[_i] := Equation(Afrund_(_ys[_i], _round));
            end if;
        end do;
        
        _T := Table(Column()$(_n+2), Row(_x_navn, op(_xs)), Row(_y_navn, op(_ys)));
        DocumentTools:-InsertContent(Worksheet(_T));
    end use;
    NULL;
end proc;


###################
# Gem
#
# Gemmer en variabel på computeren
#
# - navn er navnet på en variabel
# - as er et tekststreng med et alternativt filnavn
#
Gem := proc(navn::evaln, as::string := "")
    local _navn;
    _navn := navn;
    if not as = "" then
        _navn := as;
    end if;
    save navn, SaveName_(_navn);
end proc;


###################
# Hent
#
# Henter en variable gemt med Gem
#
# - navn er det navn eller den tekststreng, som variablen blev gemt under
#
Hent := proc(navn::evaln)
    read SaveName_(navn);
end proc;
    

###################
# LøsDiff(dif-lign [, startbetingelse, ...] [, uafhængige = var] [, numerisk])
#
# Løser en differentialligning
#
# - lign er en sekvens af ligninger og initialbetingelser
# - uafhængige er den uafhængige variable
# - numerisk angiver at ligningen skal løses numerisk (ej implementeret!)
#
LøsDiff := proc(lign::seq(equation), {uafhængige := 'x'})
    local L_, f_;
    if HasOption_('numerisk', _rest) then
        L_ := dsolve([lign], 'numeric', 'output'='operator');
        f_ := unapply(rhs(L_[2])('x'), 'x');
        f_;
    else
        L_ := dsolve([lign]);
        if type(L_, set) then
            L_ := op(L_);
        end if;
        Print_(Line_(L_));
        L_ := rhs(L_);
        L_ := unapply(L_, uafhængige);
        eval(L_);
    end if;
end proc;


###################
# Afledet(udtryk)
#
# Returnerer den afledede funktion
#
# - udtryk er et funktionsudtryk i en variabel
#
Afledet := proc(f::Or(procedure, algebraic))
    local var_, f_;
    
    if nargs > 1 then
        var_ := _rest[1];
    else
        var_ := x;
    end if;
    if type(f, 'procedure') then
        f_ := f;
    else
        f_ := unapply(f, var_);
    end if;
    unapply(diff(f_(var_), var_), var_);
end proc;


###################
# Stamfunktion(udtryk, opts)
#
# Returnerer stamfunktionen
#
# - udtryk er et funktionsudtryk i en variabel
# - opts er symbolet for den uafhængige variabel eller 'konstant'=symbol,
#   hvor symbol er navnet på et konstantled (def. = k)
#
Stamfunktion := proc(func::Or(procedure, algebraic))
    local var_, f_, konst_, rest_;
  
    konst_, rest_ := GetOption_('konstant', _rest, _def = 'k');
    if nops(rest_) > 0 then
        var_ := rest_[1];
    else
        var_ := x;
    end if;
    if type(func, 'procedure') then
        f_ := func;
    else
        f_ := unapply(func, var_);
    end if;
    unapply(int(f_(var_), var_) + konst_, var_);
end proc;


###################
# PartielAfledet( udtryk, var) 
#
# Returnerer den partielle afledede
#
# - udtryk er et funktionsudtryk i x og y
# - var er en af x og y
#
PartielAfledet := proc(f::Or(procedure, algebraic), var)
    local f_;

    if type(f, 'procedure') then
        f_ := f;
    else
        f_ := unapply(f, 'x', 'y');
    end if;
    unapply(diff(f_('x', 'y'), var), 'x', 'y');
end proc;
