#############################################################################
#                                                                           #
# Copyright (C) 2018 by Claus Christensen                                   #
# This file is part of KGModul                                              #
#                                                                           #
# This program is free software: you can redistribute it and/or modify      #
# it under the terms of the GNU General Public License as published by      #
# the Free Software Foundation, either version 3 of the License, or         #
# (at your option) any later version.                                       #
#                                                                           #
# This program is distributed in the hope that it will be useful,           #
# but WITHOUT ANY WARRANTY; without even the implied warranty of            #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
# GNU General Public License for more details.                              #
#                                                                           #
# You should have received a copy of the GNU General Public License         #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.    #
#                                                                           #
#############################################################################

###################
# MakeVector_(x)
#
# Lave n x 1- og 1 x n-matricer om til vektorer
# Nødvendigt for at kunne håndtere hastighedsvektorer
#
MakeVector_ := proc( x::{Vector, Matrix})
    if type(x, Vector) then
        return x;
    end if;
    if op(1, x)[1] > 1 and op(1, x)[2] > 1 then
        Err_("Argumentet skal være en vektor");
    end if;
    convert(x, Vector);
end proc;

###################
# Tværvektor(x)
#
# Finder tværvektoren til vektor x
#
Tværvektor := proc( x::{Vector, Matrix})
    local x_, y_;
    x_ := MakeVector_(x);
    if not op(1, x_) = 2 then
        Err_("Argumentet skal være en vektor i planen");
    end if;
    if type(x_, 'Vector[row]') then
        y_ := Vector[row](2);
    else
        y_ := Vector[column](2);
    end if;
    y_[1] := -x_[2];
    y_[2] := x_[1];
    y_;
end proc;


###################
# Skalarprodukt(a, b)
#
# Finder skalarproduktet imellem vektorerne a og b
#
Skalarprodukt := proc( a::{Vector, Matrix}, b::{Vector, Matrix})
    local i_, a_, b_;
    a_ := MakeVector_(a);
    b_ := MakeVector_(b);
    if not op(1,a_) = op(1,b_) then
        error("Vektorerne skal have samme dimension");
    end if;
    add( a_[i_]*b_[i_], i_=1..op(1, a_));
end proc;


###################
# Længde(x)
#
# Finder længden af vektor x
#
Længde := proc( x::{Vector, Matrix})
    local x_;
    x_ := MakeVector_(x);
    sqrt(Skalarprodukt(x_, x_));
end proc;


###################
# Determinant(a, b)
#
# Finder determinanten for et par af plane vektorer
#
Determinant := proc( a::{Vector, Matrix}, b::{Vector, Matrix})
    local a_, b_;
    a_ := MakeVector_(a);
    b_ := MakeVector_(b);
    if not op(1, a_) = 2 or not op(1, b_) = 2 then
        error("Du kan kun beregne en determinant for vektorer i planen");
    end if;
    Skalarprodukt(Tværvektor(a_), b_);
end proc;

###################
# Vektorprodukt(a, b)
#
# Finder vektorproduktet imellem vektorerne a og b
#
Vektorprodukt := proc( a::{Vector, Matrix}, b::{Vector, Matrix})
    local a_, b_;
    a_ := MakeVector_(a);
    b_ := MakeVector_(b);
    if not op(1, a_) = 3 or not op(1, b_) = 3 then
        error("Du kan kun beregne et vektorprodukt for vektorer i rummet");
    end if;
    LinearAlgebra[CrossProduct](a_, b_);
end proc:


###################
# Projektion(b, a)
#
# Finder projektionen af vektor b på vektor a
#
Projektion := proc(b::{Vector, Matrix}, a::{Vector, Matrix})
    local a_, b_;
    a_ := MakeVector_(a);
    b_ := MakeVector_(b);
    if not op(1, a_) = op(1, b_) then
        error("Vektorerne skal have samme dimension");
    end if;
    Skalarprodukt(a_, b_) / Længde(a_)^2 * a_;
end proc;


###################
# distPP_(v1, v2)
#
# Find afstanden imellem to punkter givet ved deres stedvektorer
#
distPP_ := proc(v1::Vector, v2::Vector)
    Længde(v2 - v1);
end proc;


###################
# distPl_(P, a, P0)
#
# Find afstanden fra punktet med stedvektor P til linjen givet ved et 
#  punkt P0 og en retningsvektor a
#
distPl_ := proc(P::Vector, a::Vector, P0::Vector)
    local Q; 
    Q := P0 + Projektion(P - P0, a);
    distPP_(P, Q);
end proc;


###################
# distll_(a, P0, b, Q0)
#
# Afstanden imellem to linjer hver givet ved en retningsvektor og et punkt
#
distll_ := proc(a::Vector, P0::Vector, b::Vector, Q0::Vector)
    local a2, b2, ab, apq0, bpq0, t, s, P, Q;
    
    # linjer i planen
    if op(1, a) = 2 then 
        if Skalarprodukt(a, Tværvektor(b)) = 0 then # parallelle linjer
            return distPl_(P0, b, Q0);
        else
            return 0;
        end if;
    end if;
    
    # Linjer i rummet
    if Længde(Vektorprodukt(a,b)) = 0 then
        return distPl_(P0, b, Q0);
    end if;
    
    # ikke-parallelle linjer
    a2 := Skalarprodukt(a, a);
    b2 := Skalarprodukt(b, b);
    ab := Skalarprodukt(a, b);
    apq0 := Skalarprodukt(a, P0 - Q0);
    bpq0 := Skalarprodukt(b, P0 - Q0);
    t := (b2 * apq0 - ab * bpq0) / (ab^2 - a2*b2);
    s := (ab * apq0 - a2 * bpq0) / (ab^2 - a2*b2);
    P := a * t + P0;
    Q := b * s + Q0;
    distPP_(P, Q);
end proc;


###################
# distPa_(P, n, P0)
#
# Afstand imellem punkt P og plan givet ved normalvektor n og punkt P0
#
distPa_ := proc(P::Vector, n::Vector, P0::Vector)
    Længde(Projektion(P - P0, n));
end proc;


###################
# repr_(x [, 'variable'=vars])
#
# Standard repræsentation af punkt, linje eller plan til brug for 
#  _dist-funktionerne
# x     - en ligning, en stedvektor eller en vektor-parameterfremstillig
# vars  - mængde eller liste med de(n) variable
#
repr_ := proc(x)
    local n, r, P, L, L1, L2, a, b, c, d, n_vec, vars;
    if type(x, `=`) then
        vars := FindOption_('variable', _rest);
        if vars = FAIL then
            vars := indets(x);
        end if;
        L := rhs(x) - lhs(x);
        L1 := coeff(L, vars[1], 0);
        a := coeff(L, vars[1], 1);
        b := coeff(L1, vars[2], 1);
        if nops(vars) = 2 then # linje i 2D - rep: retningsvektor, punkt
            c := coeff(L1, vars[2], 0);
            if a = 0 then
                P := <0, -c / b>;
            else
                P := <-c / a, 0>;
            end if;
            return ['linje', <-b, a>, P];
        else                        # plan - rep: normalvektor, punkt
            L2 := coeff(L1, vars[2], 0);
            c := coeff(L2, vars[3], 1);
            d := coeff(L2, vars[3], 0);
            if a = 0 then
                if b = 0 then
                    P := <0, 0, -d / c>;
                else
                    P := <0, -d / b, 0>;
                end if;
            else
                P := <-d / a, 0, 0>;
            end if;
            return ['plan', <a, b, c>, P];
        end if;
    else # parameterfremstilling
        vars := FindOption_('parametre', _rest);
        if vars = FAIL then
            vars := indets(x);
        else
            if type(vars, 'list') then
                vars := convert(vars, 'set');
            end if;
            vars := vars intersect indets(x);
        end if;
        n :=  nops(vars);
        if n = 0 then
            return ['punkt', x];
        elif n = 1 then
            return ['linje', coeff(x, vars[1], 1), coeff(x, vars[1], 0)];
        else
            n_vec := Vektorprodukt(coeff(x, vars[1], 1), coeff(x, vars[2], 1));
            P := coeff(coeff(x, vars[1], 0), vars[2], 0);
            return ['plan', n_vec, P];
        end if;
    end if;
end proc;


###################
# Afstand(a, b [, opt])
#
# Afstand imellem geometriske objekter
# a, b  - stedvektor eller ligning eller vektor-parameterfremstillig for 
#           linje eller plan
# opt   -   liste med uafhængige variable
#
Afstand := proc(a, b)
    local vars, rep_a, rep_b;
    if not HasOption_('variable', _rest) and type(a, `=`) and type(b, `=`) then
        vars := indets(a) union indets(b);
        rep_a := repr_(a, 'variable' = vars);
        rep_a := repr_(a, 'variable' = vars);
    else
        rep_a := repr_(a, _rest);
        rep_b := repr_(b, _rest);
    end if;
    
    # a repræsenterer et punkt
    if rep_a[1] = 'punkt' then
        if rep_b[1] = 'punkt' then
            return distPP_(a, b);
        elif rep_b[1] = 'linje' then
            return distPl_(a, op(2..3, rep_b));
        else
            return distPa_(a, op(2..3, rep_b));
        end if;
        
    # a repræsenterer en linje
    elif rep_a[1] = 'linje' then
        if rep_b[1] = 'punkt' then
            return distPl_(b, op(2..3, rep_a));
        elif rep_b[1] = 'linje' then
            return distll_(op(2..3, rep_a), op(2..3, rep_b));
        else
            if not Skalarprodukt(rep_a[2], rep_b[2]) = 0 then
                return 0;
            else
                return distPa_(rep_a[3], op(2..3, rep_b));
            end if;
        end if;
        
    # a repræsenterer et plan
    else
        if rep_b[1] = 'punkt' then
            return distPa_(b, op(2..3, rep_a));
        elif rep_b[1] = 'linje' then
            if not Skalarprodukt(rep_a[2], rep_b[2]) = 0 then
                return 0;
            else
                return distPa_(rep_b[3], op(2..3, rep_a));
            end if;
        else
            if not Længde(Vektorprodukt(rep_a[2], rep_b[2])) = 0 then
                return 0;
            else
                return distPa_(rep_a[3], op(2..3, rep_b));
            end if;
        end if;
    end if;
end proc;

