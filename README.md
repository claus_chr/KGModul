# KGModul
En Maple-pakke til brug for gymnasiets matematik-, fysik- og kemiundervisning

KGModul indeholder funktioner, som gør det lettere for elever at arbejde med 
Maple og at redegøre for deres arbejde og vise resultater på en ordentlig måde. 
Alle funktionerne er dokumenterede i Maples hjælpesystem.

KGModul er fri software, copyright (C) 2011 Claus Christensen og udgivet under 
GNU General Public Licence version 3.0 eller senere.

## Installation af KGModul

KGModul installeres via MapleCloud. Start Maple op og klik på sky-ikonet i 
øverste højre hjørne. Find KGModul under Packages og klik på 
installations-ikonet. Når pakken er installeret kan dens funktionalitet bruges i 
ethvert Maple-dokument efter kommandoen `with(KGModul)`. 

Det er ikke nødvendigt at have en konto til MapleCloud for at installere KGModul.

Når pakken installeres, så installeres også hjælp til dens kommandoer.
Der er også en oversigt over KGModul med links til siderne for pakkens forskellige kommandoer; 
skriv `?KGModul,KGModul` i et Maple-dokument for at se oversigten.

Første gang pakken bruges bør kommandoen `Init()` køres. Den laver en ini-fil 
med nogle praktiske indstillinger. 

Bemærk, at Maple-versioner før Maple 2018 kan have problemer med at installere pakker fra MapleCloud. 

### Hvis MapleCloud giver problemer

KGModul kan installeres direkte fra workbook'en KGModul.maple, fx ved at lægge 
den og filen InstallerKGModul.mw på skrivebordet, åbne InstallerKGModul.mw i 
Maple og genberegne hele dokumentet.

## Byg KGModul fra kildekoden

Download alle filerne i projektet og bevar mappestrukturen. Åbn filen 
BuildPackage.mw i Maple og genberegn den. Derved laves en Maple-workbook ved 
navn KGModul.maple, som kan åbnes i Maple. Fra KGModul version 2.2 afhænger 
kildekoden af funktioner fra Maple 2019. Hvis du har problemer med at få KGModul 
til at virke på din maskine, så skal du måske opdatere din Maple-installation.

## Opdatering til en nyere version

MapleCloud ikonet vil vise, når en ny version bliver udgivet. Klik på ikonet, 
gå til Updates og download den nyeste version herfra. Installationen foregår 
automatisk.

